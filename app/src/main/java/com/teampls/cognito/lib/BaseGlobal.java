package com.teampls.cognito.lib;

public class BaseGlobal {
    public static final int RequestPermission = 9989;
    public static final String TermsOfServiceUrl = "https://s3.ap-northeast-2.amazonaws.com/sps-res-public/terms-and-policies/terms-of-service.html";
    public static final String PrivacyPolicyUrl = "https://s3.ap-northeast-2.amazonaws.com/sps-res-public/terms-and-policies/privacy-policy.html";

}
