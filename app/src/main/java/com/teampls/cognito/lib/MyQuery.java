package com.teampls.cognito.lib;

import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;
import android.util.Log;
import android.util.Pair;

import java.util.ArrayList;
import java.util.List;

public class MyQuery {
    static final public String BLOB = "BLOB";
    static final public String INT = "INTEGER";
    static final public String TEXT_TYPE = "TEXT NOT NULL";

    // Between : SELECT * FROM tableName WHERE a BETWEEN 10 AND 30 (원하는 날짜에 대해서 조회시)
    // IN : SELECT * FROM tableName WHERE a IN(10,30) (원하는 itemId에 대해서만 조회시)
    // LIKE : SELECT * FROM tableName WHERE a LIKE '%hi%' (단어 조회시)
    // >= :  SELECT * FROM tableName WHERE a >= 20;


    public static String createSelection(Enum<?> column, String inequality, String value) {
        return String.format("%s %s %s", column.toString(), inequality, value);
    }

    public static Pair<String, String[]> createSelectionBETWEEN(Enum<?> column, String from, String to) {
        String selection = String.format("%s BETWEEN ? AND ?", column.toString());
        String[] selectionArg = {from, to};
        return Pair.create(selection, selectionArg);
    }

    public static List<String> createQuestionArray(List selectionArg) {
        List<String> results = new ArrayList<>();
        for (Object obj : selectionArg)
            results.add("?");
        return results;
    }

    public static String selectWildcard(String tableName, String column) {
        return String.format("SELECT * FROM %s WHERE %s LIKE ?", tableName, column);
    }

    public static String create(String tableName, String[] columns) {
        return create(tableName, columns, -1, "", "");
    }

    public static String create(String tableName, String[] columns, String[] columnAttrs) {
        List<String> messages = new ArrayList<>();
        String query = String.format("CREATE TABLE %s ( %s INTEGER PRIMARY KEY AUTOINCREMENT", tableName, columns[0]);
        for (int i = 1; i < columns.length; i++) {
            query = query + String.format(", %s %s", columns[i], columnAttrs[i]);
            switch (columnAttrs[i]) {
                case BLOB:
                case INT:
                    messages.add(String.format("%s (%s)", columns[i], columnAttrs[i]));
                    break;
                default:
                    break;
            }
        }
        query = query + ");";
       // Log.w("DEBUG_JS", String.format("[%s.create] %s", tableName, TextUtils.join(",", messages)));
        return query;
    }

    public static String create(String tableName, String[] columns, int foreignInx, String foreignTable, String foreignColumn) {
        String query = String.format("CREATE TABLE %s ( %s INTEGER PRIMARY KEY AUTOINCREMENT", tableName, columns[0]);
        for (int i = 1; i < columns.length; i++) {
            if (i == foreignInx) {
                query = query + String.format(", FOREIGN KEY(%s) REFERENCES %s(%s)", columns[i], foreignTable, foreignColumn);
            } else {
                query = query + String.format(", %s %s", columns[i], getAttribution(columns[i]));
            }
        }
        query = query + ");";
        return query;
    }
    // CREATE TABLE myTable ( id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT_TYPE, age INTEGER_TYPE);

    public static String addColumn(String tableName, String columnName) {
        if  (getDefault(getAttribution(columnName)).isEmpty()) {
            return String.format("ALTER TABLE %s ADD COLUMN %s TEXT", tableName, columnName);
        } else{
            return String.format("ALTER TABLE %s ADD COLUMN %s %s DEFAULT %s", tableName, columnName, getAttribution(columnName), getDefault(getAttribution(columnName)));
        }
    }

    // ALTER TABLE myTable ADD COLUMN height INTEGER_TYPE DEFAULT "0"

    public static String renameTable(String tableName, String newTableName) {
        return String.format("ALTER TABLE %s RENAME TO %s", tableName, newTableName);
    }
    // ALTER TABLE myTable RENAME TO oldTable

    public static String tableExists(String tableName) {
        return String.format("SELECT name FROM sqlite_master WHERE name = '%s'", tableName);
    }

    public static String updateColumnValue(String tableName, String columnName, String value) {
        return String.format("UPDATE %s SET %s = %s", tableName, columnName, value);
    }

    private static String getColumns(ArrayList<String> columns) {
        String result = "";
        switch (columns.size()) {
            case 0:
                break;
            case 1:
                result = columns.get(0);
                break;
            default:
                int count = 0;
                for (String column : columns) {
                    if (count == 0) {
                        result = column;
                    } else {
                        result = String.format("%s, %s", result, column);
                    }
                    count = count + 1;
                }
                break;
        }
        return result;
    }

    public static String dump(String oldTable, ArrayList<String> oldColumns, String newTable, ArrayList<String> newColumns) {
        return String.format("INSERT INTO %s (%s) SELECT %s FROM %s", newTable, getColumns(newColumns), getColumns(oldColumns), oldTable);
    }

    public static boolean isIntegerType(Enum<?> column) {
        return (getAttribution(column.toString()).equals(INT));
    }

    private static String getAttribution(String columnName) {
        if ((columnName.length() >= 5) && (columnName.toLowerCase().contains("path") == false)
                && (columnName.substring(0, 5).equals("image"))) {
            // image ... OK,   imageThumb ... OK, imagePath ... -> TEXT, imageId
            Log.w("DEBUG_JS", String.format("[MyQuery.getAttribution] %s --> BLOB_TYPE", columnName));
            return BLOB;
        } else if ((columnName.length() >= 3) && (columnName.substring(0, 3).equals("int"))) {
            Log.w("DEBUG_JS", String.format("[MyQuery.getAttribution] %s --> INTEGER_TYPE", columnName));
            return INT;
        } else {
            return TEXT_TYPE;
        }
    }

    /*
            List<String> blobFields = Arrays.asList(new String[]{"image", "imageThumb"});
        List<String> integerFields = Arrays.asList(new String[]{"itemId", "amount", "contentId", "imageId", "slipItemDbId",
                "externalSlipItemDbId", "price", "serialNum", ""});
        if (blobFields.contains(columnName)) {
            Log.w("DEBUG_JS", String.format("[MyQuery.getAttribution] %s --> BLOB_TYPE"));
            return BLOB_TYPE;
        } else if (integerFields.contains(columnName)) {
            Log.w("DEBUG_JS", String.format("[MyQuery.getAttribution] %s --> INTEGER_TYPE"));
            return INTEGER_TYPE;
        } else {
            return TEXT_TYPE;
        }
     */

    private static String getAttributionLegacy(String columnName) {
        if ((columnName.length() >= 5) && (columnName.substring(0, 5).equals("image"))) {
            return BLOB;
        } else if ((columnName.length() >= 2) && (columnName.substring(0, 2).equals("is"))) { // 0 ~ 1 = 2 character
            return INT;
        } else if ((columnName.length() >= 2) && (columnName.substring(0, 2).equals("id"))) {
            return INT;
        } else if ((columnName.length() >= 3) && (columnName.substring(0, 3).equals("int"))) {
            return INT;
        } else if ((columnName.length() >= 3) && (columnName.substring(0, 3).equals("has"))) {
            return INT;
        } else {
            return TEXT_TYPE;
        }
    }

    private static String getDefault(String attribution) {
        if (attribution.equals(INT)) {
            return "0";
        } else {
            return "";
        }
    }

    public static String delete(String tableName) {
        return "DROP TABLE IF EXISTS " + tableName;
    }

    public static String getTableInfo(String tableName) {
        return String.format("PRAGMA table_info(%s)", tableName);
    }

    public static int getVersion(SQLiteDatabase database) {
        return ((Long) DatabaseUtils.longForQuery(database, "PRAGMA user_version", null)).intValue();
    }

    class ForeignKey {
        String column, refTable, refColumn;

        public ForeignKey(String column, String refTable, String refColumn) {
            this.column = column;
            this.refColumn = refColumn;
            this.refTable = refTable;
        }
    }
}


/*
          Cursor cursor = SQLiteDatabase.openOrCreateDatabase(":memory:", null).rawQuery("select sqlite_version() AS sqlite_version", null);
		ArrayList<String> sqliteVersion = new ArrayList<String>(0);
		while(cursor.moveToNext()){
		   sqliteVersion.addInteger(cursor.getString(0));
		}
		int oldVersion = Integer.valueOf(sqliteVersion.getSaved(0).substring(0, 1));
 */
