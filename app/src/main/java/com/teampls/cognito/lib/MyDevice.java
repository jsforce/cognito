package com.teampls.cognito.lib;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Surface;
import android.view.WindowManager;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import androidx.core.content.ContextCompat;

public class MyDevice {
    public static final String rootDir = Environment.getExternalStorageDirectory().getAbsolutePath();
    public static final String teamplDir = rootDir + "/" + "TEAMPL";

    public static void openWeb(Context context, String url) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(url));
        context.startActivity(intent);
    }

    public static boolean isRunning(Context context, String packageName) {
        ActivityManager activityManager = (ActivityManager) context.getSystemService(Activity.ACTIVITY_SERVICE);
        for (ActivityManager.RunningAppProcessInfo appProcessInfo : activityManager.getRunningAppProcesses()) {
            if (appProcessInfo.processName.contains(packageName))
                return true;
        }
        return false;
    }

    public static boolean hasPackage(Context context, String packageName) {
        try {
            Context otherContext = context.createPackageContext(packageName, Context.CONTEXT_IGNORE_SECURITY);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    public static void openActivity(Context context, String packageName, String activityName) {
        if (MyDevice.hasPackage(context, packageName)) {
            Intent intent = context.getPackageManager().getLaunchIntentForPackage(packageName);
            if (!TextUtils.isEmpty(activityName) && isRunning(context, packageName)) {
                try {
                    Intent activeIntent = new Intent(activityName);
                    activeIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    context.startActivity(activeIntent); // 실제 없는 경우 null이 아니라 여기서 No Activity found  에러가 발생
                } catch (Exception e) {
                    Log.e("DEBUG_JS", String.format("[MyDevice.openActivity] %s", e.getLocalizedMessage()));
                    context.startActivity(intent);
                }
            } else {
                //  Log.i("DEBUG_JS", String.format("[MyDevice.openActivity] acitivityName = %s, isRunning? %s", TextUtils.isEmpty(activityName), isRunning(context, packageName)));
                context.startActivity(intent);
            }
        } else {
            MyDevice.openPlayStore(context, packageName);
        }
    }

    public static void openPlayStore(Context context, String packageName) {
        try {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + packageName)));
        } catch (android.content.ActivityNotFoundException anfe) {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + packageName)));
        }
    }


    public static void openApp(Context context, String packageName) {
        openActivity(context, packageName, "");
    }

    public static List<UserPermission> getAllPermissions() {
        List<UserPermission> results = new ArrayList<>();
        results.add(UserPermission.PHONE);
        results.add(UserPermission.WRITE_STORAGE);
        results.add(UserPermission.CAMERA);
        results.add(UserPermission.READ_CONTACTS);
        results.add(UserPermission.CALL_PHONE);
        return results;
    }

    public static boolean hasAllPermissions(Context context) {
        for (UserPermission userPermission : getAllPermissions()) {
            if (userPermission == UserPermission.CALL_PHONE)
                continue; // 전화기가 없는 경우 (태블릿, 시뮬레이터) 문제 발생
            if (hasPermission(context, userPermission) == false) {
                Log.e("DEBUG_JS", String.format("[MyDevice.hasAllPermissions] %s not allowed", userPermission.toString()));
                return false;
            }
        }
        return true;
    }

    public static void refresh(Context context, File file) {
        new MediaUpdater(context, file, null).refresh();
    }

    public static void createFolderIfNotExist(File folder) {
        try {
            if (folder.exists() == false) {
                boolean success = folder.mkdirs();
                Thread.sleep(1);   // to avoid an error: open failed: EBUSY (Device or resource busy)
                if (success) {
                    Log.i("DEBUG_JS", String.format("[%s.createFolderIfNotExist] %s is created", MyDevice.class.getSimpleName(), folder.toString()));
                } else {
                    Log.e("DEBUG_JS", String.format("[%s.createFolderIfNotExist] %s is not created", MyDevice.class.getSimpleName(), folder.toString()));
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    public static void createFolders(Context context, String folderPath) {
        folderPath = folderPath + "/readme.txt";
        createFolderIfNotExist(new File(folderPath));
        refresh(context, new File(folderPath));
    }

    public static String[] getAllPermissionStr() {
        return new String[]{Manifest.permission.READ_PHONE_STATE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.CAMERA,
                Manifest.permission.READ_CONTACTS,
                Manifest.permission.CALL_PHONE
        };
    }

    public static void openAppSetting(final Context context) {
        Intent intent = new Intent();
        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", context.getPackageName(), null);
        intent.setData(uri);
        ((Activity) context).startActivityForResult(intent, BaseGlobal.RequestPermission);
    }


    public static int toPixel(Context context, int dp) {
        if (dp < 0)
            return dp;
        Resources r = context.getResources();
        float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics());
        return (int) px;
    }


    public static boolean isTablet(Context context) {
        return (getWidthDP(context) >= DeviceSize.W600.size);
    }

    public static float getFontScale(Context context) {
        return context.getResources().getConfiguration().fontScale;
    }

    public static float toOriginalSP(Context context, float px) {
        float scale = MyDevice.getFontScale(context);
        float sp = toSP(context, px);
        if (scale == 0)
            return sp;
        else
            return sp / scale;
    }

    private static float toSP(Context context, float px) {
        if (px <= 0)
            return 0;
        return px / context.getResources().getDisplayMetrics().density;
    }


    public enum DeviceSize {
        W320(320), W360(360), W383(383), W400(400), W411(411), W600(600), W720(720), W800(800), W1000(1000);
        public int size = 0;

        DeviceSize(int size) {
            this.size = size;
        }
    }

    public static DeviceSize getDeviceSize(Context context) {
        int myDeviceWidthDp = 0;
        if (isLandscapeScreen(context)) {
            myDeviceWidthDp = getHeightDP(context);
        } else {
            myDeviceWidthDp = getWidthDP(context);
        }
        for (DeviceSize deviceSize : DeviceSize.values()) {
            if (myDeviceWidthDp <= deviceSize.size) { // 가장 근접한 것 중에서 큰 것
                return deviceSize;
            }
        }
        return DeviceSize.W360;
    }


    public static boolean isLandscapeScreen(Context context) {
        final int screenOrientation = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getOrientation();
        switch (screenOrientation) {
            // portrait
            case Surface.ROTATION_0:
            case Surface.ROTATION_180:
                return false;
            // landscape
            case Surface.ROTATION_90:
            default:
                return true;
        }
    }

    public static int getHeight(Context context) {
        DisplayMetrics dm = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(dm);
        if (isLandscapeScreen(context)) {
            return Math.min(dm.widthPixels, dm.heightPixels);
        } else {
            return dm.heightPixels;
        }
    }

    public static int toDP(Context context, int px) {
        if (px <= 0)
            return px;
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float dp = px / (metrics.densityDpi / 160f);
        return (int) dp;
    }

    public static int getHeightDP(Context context) {
        return toDP(context, getHeight(context));
    }

    public static boolean isLessThan(Context context, DeviceSize refSize) {
        if (isLandscapeScreen(context)) {
            return (getHeightDP(context) <= refSize.size);
        } else {
            return (getWidthDP(context) <= refSize.size);
        }
    }

    public static int getWidthDP(Context context) {
        return toDP(context, getWidth(context));
    }

    public static int getWidth(Context context) {
        DisplayMetrics dm = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(dm);
        if (isLandscapeScreen(context)) {
            return Math.max(dm.widthPixels, dm.heightPixels);
        } else {
            return dm.widthPixels;
        }
    }


    public static int getAndroidVersion() {
        return Build.VERSION.SDK_INT;
    }

    @SuppressLint("MissingPermission")
    public static String getMyPhoneNumber(Context context) {
        try {
            if (hasPermission(context, UserPermission.CALL_PHONE) == false) {
                MyUI.toastSHORT(context, String.format("전화번호 가져오기 권한이 없습니다"));
                return "";
            }
            if (context == null)
                return "";
            TelephonyManager manager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            if (manager == null)
                return "";

            String phoneNumber = manager.getLine1Number();
            if (phoneNumber == null) {
                return "";
            } else {
                return BaseUtils.toSimpleForm(phoneNumber);
            }
        } catch (Exception e) {
            return "";
        }
    }

    public enum UserPermission {CAMERA, PHONE, WRITE_STORAGE, READ_STORAGE, READ_CONTACTS, WRITE_CONTACTS, CALL_PHONE}


    public static boolean hasPermission(final Context context, final UserPermission userPermission) {
        boolean result = false;
        switch (userPermission) {
            case PHONE:
                result = (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED);
                break;
            case CAMERA:
                result = (ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED);
                break;
            case WRITE_STORAGE:
                result = (ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);
                break;
            case READ_STORAGE:
                result = (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);
                break;
            case READ_CONTACTS:
                result = (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED);
                break;
            case WRITE_CONTACTS:
                result = (ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_CONTACTS) == PackageManager.PERMISSION_GRANTED);
                break;
            case CALL_PHONE:
                result = (ContextCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED);
                break;
        }
        // Log.i("DEBUG_JS", String.format("[MyDevice.hasPermission] %s : %s ", userPermission.toDbString(), Boolean.toDbString(result)));
        return result;
    }

}
