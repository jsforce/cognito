package com.teampls.cognito.lib;

import android.util.Log;


public class MyRecord {
    public int id = 0;
    public String username = "", phoneNumber = "", nickname = "";
	public String password = "";
    public boolean confirmed = false;
	public MyRecord() {}
	
	public MyRecord(int id, String username, String phoneNumber, String nickname, String password, boolean confirmed) {
		this.id = id;
		this.username = username;
		this.phoneNumber = phoneNumber;
		this.nickname = nickname;
		this.password = password;
        this.confirmed = confirmed;
	}
	
	public MyRecord(String username, String phoneNumber, String nickname, String password) {
		this.username = username;
		this.phoneNumber = phoneNumber;
		this.nickname = nickname;
		this.password = password;
	}

    public MyRecord(String phoneNumber, String password, boolean confirmed) {
        this.phoneNumber = phoneNumber;
        this.password = password;
        this.confirmed = confirmed;
    }


    public void toLogCat(String callLocation) {
        Log.w("DEBUG_JS", String.format("[%s] username %s, phoneNumber %s, nickname %s, password %s*****, ? %s",
				callLocation, username, phoneNumber, nickname, password, confirmed));
    }
}
