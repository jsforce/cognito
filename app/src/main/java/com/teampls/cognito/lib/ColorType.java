package com.teampls.cognito.lib;

import android.graphics.Color;
import android.text.TextUtils;
import android.util.Log;


import java.util.ArrayList;
import java.util.List;

public enum ColorType {
    //http://html-color-codes.info/Korean/
    //https://www.mathworks.com/matlabcentral/mlc-downloads/downloads/submissions/24497/versions/2/screenshot.PNG

    //=== Main Color =======
    Default("기본", ""),
    White("화이트", "#FFFFFF"),
    Ivory("아이보리", "#FFFFF0"),
    LightGrey("연그레이", "#D3D3D3"),
    Silver("실버", "#C0C0C0"),
    Gray("그레이", "#808080"),
    CharcoalGray("차콜", "#36454F"),
    Beige("베이지", "#F5F5DC"),
    LemonYellow("레몬", "#FFF44F"),
    Yellow("엘로우", "#FFFF00"),
    Mustard("머스타드", "#FFDB58"),
    Gold("골드", "#FFD700"),
    Orange("오랜지", "#FFA500"),
    Pink("핑크", "#FFC0CB"),
    IndiePink("인디핑크", "#FD9BCD"),
    HotPink("핫핑크", "#FF69B4"),
    FireBrick("벽돌", "#B22222"),
    Brown("브라운", "#A52A2A"),
    Burgundy("버건디", "#800020"),
    MintGreen("민트", "#98FF98"),
    LightGreen("연두", "#90EE90"),
    Green("그린", "#008000"),
    KhakiGreen("카키", "#579244"),
    Camel("카멜", "#C19A6B"),
    Red("레드", "#FF0000"),
    SkyBlue("하늘", "#87CEEB"),
    Blue("블루", "#0000FF"),
    Navy("네이비", "#000080"),
    Purple("보라", "#800080"),
    Black("블랙", "#000000"),
    RoyalBlue("로얄블루", "#4169E1"),
    LightSlateGray("슬레이트", "#778899"),


    //=== 기타 =======
    IndianRed("인디언레드", "#CD5C5C"),
    Khaki("카키", "#F0E68C"),
    OliveDrab("올리브", "#6B8E23"),
    DarkGray("다크그레이", "#A9A9A9"),
    Turquoise("옥색", "#40E0D0"),
    LightSkyBlue("연하늘", "#87CEFA"),
    SteelBlue("철청", "#4682B4"),
    Maroon("밤색", "#800000"),
    WoodBark("#311926"),
    Sienna("#A0522D"),
    Indigo("#4B0082"),
    ForestGreen("#228B22"),
    Fuchsia("#FF00FF"),
    Olive("#808000"),
    Chocolate("#D2691E"),
    SlateGray("#708090"),
    RedWine("#990012"),
    CornflowerBlue_Trans("#BB5882FA"),
    Trans,
    //   ActivityDefault("#FAFAFA"),
    LimeGreen("#32CD32"),
    CheckBoxColor("#ff0099cc"),
    LightSteelBlue("#B0C4DE"),
    LightBlue("#ADD8E6"),
    LightRed("#FFCCCB"),
    Azure("#F0FFFF"),
    SandyBrown("#F4A460"),


    // == 앱 =====
    Advance("미송", "#FF8774"),
    Preorder("주문", "#FF00FF"),
    Consign("위탁", "#749FC1"),

    // == 배경색 ===
    RedTrans("#22FF0000"),
    RedTransStrong("#99FF0000"),
    WhiteTransString("#88ffffff"),
    DarkGrayTrans("#DDa9a9a9"),
    RetailColor(Navy),


    // == Flat ====
    FlatForestGreen("#284D32"), // order
    FlatOrage("#DD691B"), // exchange
    FlatRedDark("#B02620"), // returned
    FlatPowerBlueDark("#899BCD"), // warehouse
    FlatBlueDark("#2C3A6F"), // directwarehouse

    //=== 유저 =======
    USER00,
    USER01,
    USER02,
    USER03,
    USER04,
    USER05,
    USER06,
    USER07,
    USER08,
    USER09,
    USER10,
    USER11,
    USER12,
    USER13,
    USER14,
    USER15,
    USER16,
    USER17,
    USER18,
    USER19,
    USER20,
    USER21,
    USER22,
    USER23,
    USER24,
    USER25,
    USER26,
    USER27,
    USER28,
    USER29,
    USER30,
    USER31,
    USER32,
    USER33,
    USER34,
    USER35,
    USER36,
    USER37,
    USER38,
    USER39,
    USER40,
    USER41,
    USER42,
    USER43,
    USER44,
    USER45,
    USER46,
    USER47,
    USER48,
    USER49,
    USER50,
    USER51,
    USER52,
    USER53,
    USER54,
    USER55,
    USER56,
    USER57,
    USER58,
    USER59,
    USER60,
    USER61,
    USER62,
    USER63,
    USER64,
    USER65,
    USER66,
    USER67,
    USER68,
    USER69,
    USER70,
    USER71,
    USER72,
    USER73,
    USER74,
    USER75,
    USER76,
    USER77,
    USER78,
    USER79,
    USER80,
    USER81,
    USER82,
    USER83,
    USER84,
    USER85,
    USER86,
    USER87,
    USER88,
    USER89,
    USER90,
    USER91,
    USER92,
    USER93,
    USER94,
    USER95,
    USER96,
    USER97,
    USER98,
    USER99,
    USER100,
    USER101,
    USER102,
    USER103,
    USER104,
    USER105,
    USER106,
    USER107,
    USER108,
    USER109,
    USER110,
    USER111,
    USER112,
    USER113,
    USER114,
    USER115,
    USER116,
    USER117,
    USER118,
    USER119,
    USER120,
    USER121,
    USER122,
    USER123,
    USER124,
    USER125,
    USER126,
    USER127,
    USER128,
    USER129,
    USER130,
    USER131,
    USER132,
    USER133,
    USER134,
    USER135,
    USER136,
    USER137,
    USER138,
    USER139,
    USER140,
    USER141,
    USER142,
    USER143,
    USER144,
    USER145,
    USER146,
    USER147,
    USER148,
    USER149,
    USER150,
    USER151,
    USER152,
    USER153,
    USER154,
    USER155,
    USER156,
    USER157,
    USER158,
    USER159,
    USER160,
    USER161,
    USER162,
    USER163,
    USER164,
    USER165,
    USER166,
    USER167,
    USER168,
    USER169,
    USER170,
    USER171,
    USER172,
    USER173,
    USER174,
    USER175,
    USER176,
    USER177,
    USER178,
    USER179,
    USER180,
    USER181,
    USER182,
    USER183,
    USER184,
    USER185,
    USER186,
    USER187,
    USER188,
    USER189,
    USER190,
    USER191,
    USER192,
    USER193,
    USER194,
    USER195,
    USER196,
    USER197,
    USER198,
    USER199;;

    public String hex = "";
    public int textColor = Color.BLACK, red = 0, green = 0, blue = 0, colorInt = 0;
    private String uiName = "";

    ColorType() {
        this("", "");
    }

    ColorType(String hex) {
        this("", hex);
    }

    ColorType(ColorType colorType) {
        this(colorType.uiName, colorType.hex);
    }


    ColorType(String uiName, String hex) {
        if (isUserType()) {
            this.uiName = "입력" + this.toString().replace("USER", "");
            this.hex = this.toString().toLowerCase(); // 6글자 필요, 7글자면?
            return;
        }

        this.uiName = uiName.isEmpty() ? toString() : uiName;
//        if (uiStr.equals("Default"))
//            this.uiStr = "";
        this.hex = hex;
        if (TextUtils.isEmpty(hex)) {
            this.colorInt = 0;
            textColor = Color.BLACK;
        } else {
            this.colorInt = toInt(hex);
            this.red = Color.red(colorInt);
            this.green = Color.green(colorInt);
            this.blue = Color.blue(colorInt);
            textColor = getTextColor(hex);
        }

        //Log.i("DEBUG_JS",String.format("[%s] %s, Hex %s, R %d, G %d, B %d",getClass().getSimpleName(), this.toDbString(), hex, red, green, blue));
    }

    public boolean isUserType() {
        return this.toString().toLowerCase().startsWith("user"); // user10, use160
    }

    public static int getTextColor(String hex) {
        int colorInt = toInt(hex);
        if (Color.blue(colorInt) + Color.red(colorInt) + Color.green(colorInt) <= 400) {
            return Color.WHITE;
        } else {
            return Color.BLACK;
        }
    }

    public static int toInt(String hex) {
        if (TextUtils.isEmpty(hex))
            return 0;

        if (hex.toLowerCase().startsWith("user"))
            return 0;

        try {
            return Color.parseColor(hex); // #이 있는 string
        } catch (IllegalArgumentException e) {
            Log.e("DEBUG_JS", String.format("[ColorType.toInt] %s", e.getLocalizedMessage()));
            return 0;
        } catch (Exception e) {
            return 0;
        }
    }

    public static ColorType ofRemoteStr(String remoteStr) {
        if (remoteStr == null || remoteStr.isEmpty() || remoteStr.equals("null") ||
                remoteStr.equals(Default.toString()) || remoteStr.toLowerCase().equals("clnone")) {
            return Default;
        }
        // clnone (QR 코드 생성시 6칸을 채우기 위해 들어가는 문자)
        remoteStr = remoteStr.replace("#", "").toLowerCase();
        for (ColorType color : values())
            if (color.toRemoteStr().equals(remoteStr))
                return color;
        Log.e("DEBUG_JS", String.format("[ColorType.ofRemoteStr] fail to find color %s", remoteStr));
        return Default;
    }

    public static ColorType ofRemoteStrThrowE(String remoteStr) {
        if (remoteStr == null || remoteStr.isEmpty() || remoteStr.equals("null") ||
                remoteStr.equals(Default.toString()) || remoteStr.toLowerCase().equals("clnone")) {
            return Default;
        }
        // clnone (QR 코드 생성시 6칸을 채우기 위해 들어가는 문자)
        remoteStr = remoteStr.replace("#", "").toLowerCase();
        for (ColorType color : values())
            if (color.toRemoteStr().equals(remoteStr))
                return color;
        throw new NumberFormatException(String.format("invalid color : %s", remoteStr));
    }

    public String toRemoteStr() {
        switch (this) {
            case Default:
                return "";
            default:
                return hex.toLowerCase().replace("#", "");
        }
    }

    public void setUserColor(String colorName) {
        this.hex = Default.hex;
        this.colorInt = Default.colorInt;
        this.uiName = colorName;
    }

    public static List<ColorType> getMainColors(boolean extended) {
        ColorType[] mainColors = {
                Default, White, LightGrey, Silver, Gray, CharcoalGray,
                Beige, LemonYellow, Yellow, Ivory, Mustard, Gold, Orange,
                Pink, IndiePink, HotPink, FireBrick, Brown, Burgundy,
                MintGreen, LightGreen, Green, KhakiGreen, Camel, Red,
                SkyBlue, Blue, Navy, Purple, Black}; //RoyalBlue, LightSlateGray,
        List<ColorType> results = BaseUtils.toList(mainColors);

        for (ColorType colorType : values()) {
            if (extended == false && results.size() >= mainColors.length + 100)
                break; // 100개 까지만
            if (colorType.toString().contains("USER"))
                results.add(colorType);
        }
        return results;
    }

    public static ArrayList<ColorType> getAllColors() {
        ArrayList<ColorType> results = new ArrayList<>();
        for (ColorType colorType : getMainColors(true)) {
            results.add(colorType);
        }

        for (ColorType colorType : ColorType.values()) {
            if (results.contains(colorType) == false)
                results.add(colorType);
        }
        return results;
    }

    public static ColorType valueOf(String name, ColorType defaultValue) {
        try {
            return valueOf(name);
        } catch (Exception e) {
            //     ColorType result = ColorType.Default;
            //      result.setUserColor("");
            return defaultValue;
        }
    }

//    public static ColorType getValueFromKor(String korName) {
//        for (ColorType color : getAllColors()) {
//            if (color.uiName.equals(korName))
//                return color;
//            if (color.getUiName().equals(korName))
//                return color;
//        }
//
//        return ColorType.Default;
//    }

    public String getOriginalUiName() {
        return uiName;
    }

    public void toLogCat(String location) {
        Log.i("DEBUG_JS", String.format("[%s] %s (%s), %s", location, uiName, "", hex));
    }
}

/*

    Aquamarine("#7FFFD4"),
    Chartreuse("#7FFF00"),
    LawnGreen("#7CFC00"),
    MediumSlateBlue("#7B68EE"),

    SlateBlue("#6A5ACD"),
    DimGray("#696969"),
    MediumAquamarine("#66CDAA"),
    CornflowerBlue("#6495ED"),
    CadetBlue("#5F9EA0"),
    DarkOliveGreen("#556B2F"),

    MediumTurquoise("#48D1CC"),
    DarkSlateBlue("#483D8B"),
    SteelBlue("#4682B4"),
    RoyalBlue("#4169E1"),
    Turquoise("#40E0D0"),
    MediumSeaGreen("#3CB371"),
    DarkSlateGray("#2F4F4F"),
    SeaGreen("#2E8B57"),

    LightSeaGreen("#20B2AA"),
    DodgerBlue("#1E90FF"),
    MidnightBlue("#191970"),
    Aqua("#00FFFF"),
    Cyan("#00FFFF"),
    SpringGreen("#00FF7F"),
    Lime("#00FF00"),
    MediumSpringGreen("#00FA9A"),
    DarkTurquoise("#00CED1"),
    DeepSkyBlue("#00BFFF"),
    DarkCyan("#008B8B"),
    Teal("#008080"),
    DarkGreen("진녹색","#006400"),
    MediumBlue("#0000CD"),
    DarkBlue("#00008B"),
    BabyPink("#F4C2C2"),
    CherryPink("#FFB7C5"),
    SorrellBrown("#A1825A"),
    AfghanTan("#8F5C28"),
    Kokoda("#7D8051"),
    HotToddy("#A77724"),

    Whiskey("#CB8C5F"),
    AlmostFrost("#97867C"),
    TutleGreen("#363A1A"),
    BeautyBush("#EABFB3"),
    Spice("#69513C"),
    AshBrown("#48462F"),
    PinkLady("#F5D7BC"),
    ArrowTown("#7F776C"),
    DeepKhaki("#3D4031"),

        LightYellow("#FFFFE0"),
    Snow("#FFFAFA"),
    FloralWhite("#FFFAF0"),
    LemonChiffon("#FFFACD"),
    Cornsilk("#FFF8DC"),
    Seashell("#FFF5EE"),
    LavenderBlush("#FFF0F5"),
    PapayaWhip("#FFEFD5"),
    BlanchedAlmond("#FFEBCD"),
    MistyRose("#FFE4E1"),
    Bisque("#FFE4C4"),
    Moccasin("#FFE4B5"),
    NavajoWhite("#FFDEAD"),
    PeachPuff("#FFDAB9"),
    LightSalmon("#FFA07A"),
    DarkOrange("#FF8C00"),
    Coral("#FF7F50"),
    LightPink("#FFB6C1"),
    Tomato("#FF6347"),
    OrangeRed("#FF4500"),
    DeepPink("#FF1493"),

    Magenta("자홍","#FF00FF"),
    OldLace("#FDF5E6"),
    LightGoldenrodYellow("#FAFAD2"),
    Linen("#FAF0E6"),
    AntiqueWhite("#FAEBD7"),
    Salmon("#FA8072"),
    GhostWhite("#F8F8FF"),
    MintCream("민트", "#F5FFFA"),
    WhiteSmoke("#F5F5F5"),
    Wheat("#F5DEB3"),
    SandyBrown("#F4A460"),

    Honeydew("#F0FFF0"),
    AliceBlue("#F0F8FF"),
    LightCoral("#F08080"),
    PaleGoldenrod("#EEE8AA"),
    Violet("#EE82EE"),
    DarkSalmon("#E9967A"),
    Lavender("#E6E6FA"),
    LightCyan("#E0FFFF"),
    BurlyWood("#DEB887"),
    Plum("#DDA0DD"),
    Gainsboro("#DCDCDC"),
    Crimson("#DC143C"),
    PaleVioletRed("#DB7093"),
    Goldenrod("#DAA520"),
    Orchid("#DA70D6"),
    Thistle("#D8BFD8"),
        Tan("#D2B48C"),

    Peru("#CD853F"),
    MediumVioletRed("#C71585"),
        DarkKhaki("#BDB76B"),
    RosyBrown("#BC8F8F"),
    MediumOrchid("#BA55D3"),
    DarkGoldenrod("#B8860B"),
    PowderBlue("#B0E0E6"),

    PaleTurquoise("#AFEEEE"),
    GreenYellow("#ADFF2F"),

    DarkGray("#A9A9A9"),

    YellowGreen("#9ACD32"),
    DarkOrchid("#9932CC"),
    PaleGreen("#98FB98"),
    DarkViolet("#9400D3"),
    MediumPurple("#9370DB"),
    DarkSeaGreen("#8FBC8F"),
    SaddleBrown("#8B4513"),
    DarkMagenta("#8B008B"),
    DarkRed("#8B0000"),
    BlueViolet("#8A2BE2"),


 */