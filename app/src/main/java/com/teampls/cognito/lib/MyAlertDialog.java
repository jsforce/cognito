package com.teampls.cognito.lib;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.widget.TextView;


/**
 * Created by Medivh on 2016-09-03.
 */
abstract public class MyAlertDialog  {
    private String msg = "";

    public MyAlertDialog(Context context, String title, String message) {
        this(context, title, message, "Yes", "No");
        this.msg = message;
    }

    abstract public void yes();
    public void no() {};

    public MyAlertDialog(final Context context, String title, String message, String yesTitle, String noTitle) {

        DialogInterface.OnClickListener yesClick = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
                yes();
            }
        };
        DialogInterface.OnClickListener noClick = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
                no();
            }
        };

        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(message).setCancelable(false)
                .setTitle(title)
                .setPositiveButton(yesTitle, yesClick);
        if (noTitle != null)
            builder.setNegativeButton(noTitle, noClick);
        showDialog(context, builder);
    }

    private void showDialog(final Context context, final AlertDialog.Builder builder) {
        if (MyUI.isActiveActivity(context, msg) == false)
            return;
        ((Activity) context).runOnUiThread(new Runnable() {
            public void run() {
                AlertDialog dialog = builder.create();
                dialog.show();
                TextView textView = (TextView) dialog.findViewById(android.R.id.message);
                if (MyDevice.getAndroidVersion() >= 23)  // Marshmallow
                    textView.setTextSize(18);
                if (textView != null)
                    MyView.setTextViewByDeviceSize(context, textView);
                int titleId = context.getResources().getIdentifier( "alertTitle", "id", "android" );
                if (titleId > 0) {
                    TextView dialogTitle = (TextView) dialog.findViewById(titleId);
                    if (dialogTitle != null)
                        MyView.setTextViewByDeviceSize(context, dialogTitle);
                }
            }
        });
    }

}
