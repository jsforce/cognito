package com.teampls.cognito.lib;

import android.content.Context;
import android.media.MediaScannerConnection;
import android.net.Uri;

import java.io.File;

/**
 * Created by Medivh on 2016-09-05.
 */
public class MediaUpdater implements MediaScannerConnection.MediaScannerConnectionClient {
    private MediaScannerConnection connection;
    private File file;
    private MyOnTask onFinishEvent;

    public MediaUpdater(Context context, File file, final MyOnTask onFinishEvent) {
        this.file = file;
        connection = new MediaScannerConnection(context, this);
    }

    public void refresh() {
        connection.connect();
    }

    @Override
    public void onMediaScannerConnected() {
        connection.scanFile(file.getAbsolutePath(), null);
    }

    @Override
    public void onScanCompleted(String path, Uri uri) {
        connection.disconnect();
        if (onFinishEvent != null)
            onFinishEvent.onTaskDone(null);
    }
}
