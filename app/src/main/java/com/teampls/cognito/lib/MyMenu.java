package com.teampls.cognito.lib;

import android.view.MenuItem;

import com.teampls.cognito.R;

public enum MyMenu {
    close(R.drawable.action_close);

    public int index = 0, iconRes = 0, actionEnum = MenuItem.SHOW_AS_ACTION_ALWAYS;
    public String korStr = "";

    MyMenu(int resId) {
        this.index = this.ordinal();
        this.iconRes = resId;
        this.korStr = this.toString();
        if (this.korStr.startsWith("grid"))
            this.actionEnum = MenuItem.SHOW_AS_ACTION_NEVER;
    }
}
