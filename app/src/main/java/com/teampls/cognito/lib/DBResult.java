package com.teampls.cognito.lib;

import android.content.Context;


/**
 * Created by Medivh on 2016-10-04.
 */
public class DBResult {
    public int id = 0;
    public DBResultType resultType = DBResultType.FAILED;

    public DBResult() {
    }

    public DBResult(int id, DBResultType resultType) {
        this.id = id;
        this.resultType = resultType;
    }

    public void update(DBResultType resultType) {
        if (resultType.updatedOrInserted())
            this.resultType = DBResultType.UPDATED;
    }

    public void toast(Context context, String subject) {
        // ex. 항목이, 자료가, ...
        if (resultType == DBResultType.INSERTED) {
            MyUI.toastSHORT(context, String.format("%s 추가되었습니다", subject));
        } else if (resultType == DBResultType.UPDATED) {
            MyUI.toastSHORT(context, String.format("%s 수정되었습니다", subject));
        } else if (resultType == DBResultType.SKIPPED) {
        } else if (resultType == DBResultType.NOT_FOUND) {
            MyUI.toastSHORT(context, String.format("%s 기존 자료에서 발견되지 않았습니다"));
        } else if (resultType == DBResultType.INSERT_FAILED) {
            MyUI.toastSHORT(context, String.format("%s 추가 되지 않았습니다"));
        } else if (resultType == DBResultType.FAILED) {
                MyUI.toastSHORT(context, String.format("실패했습니다"));
        }
    }
}
