package com.teampls.cognito.lib;

import android.view.View;

/**
 * Created by Medivh on 2017-04-02.
 */

public interface MyOnClick<T> {
    public void onMyClick(View view, T record);
}
