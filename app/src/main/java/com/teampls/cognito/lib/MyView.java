package com.teampls.cognito.lib;

import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class MyView {
    public static final int CONTENT = ViewGroup.LayoutParams.WRAP_CONTENT;
    public static final int PARENT = ViewGroup.LayoutParams.MATCH_PARENT;
    public static final int HORIZONTAL = LinearLayout.HORIZONTAL;
    public static final int VERTICAL = LinearLayout.VERTICAL;
    public static final int NO_DIRECTION = -1;
    public static final int TOP = 0;
    public static final int BOTTOM = 1;
    public static final int LEFT = Gravity.LEFT; // 3

    public static <T> T setMargin(Context context, T view, int leftDp, int topDp, int rightDp, int bottomDp) {
        ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) ((View) view).getLayoutParams();
        params.setMargins(MyDevice.toPixel(context, leftDp), MyDevice.toPixel(context, topDp), MyDevice.toPixel(context, rightDp), MyDevice.toPixel(context, bottomDp));
        ((View) view).setLayoutParams(params);
        return view;
    }

    public static Button createButton(Context context, String text, int textSize, int padding) {
        Button button = new Button(context);
        button.setText(text);
        button.setTextSize(textSize);
        int paddingPixel = MyDevice.toPixel(context, padding);
        button.setPadding(paddingPixel, paddingPixel, paddingPixel, paddingPixel);
        button.setLayoutParams(getParams());
        return button;
    }

    public static GridLayout.LayoutParams getGridParamsWithMargin(Context context, int widthDp, int heightDp, int leftDp, int topDp, int rightDp, int bottomDp) {
        return new GridLayout.LayoutParams(getParamsWithMargin(context, widthDp, heightDp, leftDp, topDp, rightDp, bottomDp));
    }

    public static LinearLayout.LayoutParams getParamsWithMargin(Context context, int widthDp, int heightDp, int leftDp, int topDp, int rightDp, int bottomDp) {
        LinearLayout.LayoutParams params = getParams(context, widthDp, heightDp);
        return setMarginLinear(context, params, leftDp, topDp, rightDp, bottomDp);
    }

    public static LinearLayout.LayoutParams getParams(Context context, int widthDp, int heightDp) {
        return new LinearLayout.LayoutParams(MyDevice.toPixel(context, widthDp), MyDevice.toPixel(context, heightDp));
    }

    public static LinearLayout.LayoutParams setMarginLinear(Context context, LinearLayout.LayoutParams params, int leftDp, int topDp, int rightDp, int bottomDp) {
        params.setMargins(MyDevice.toPixel(context, leftDp), MyDevice.toPixel(context, topDp), MyDevice.toPixel(context, rightDp), MyDevice.toPixel(context, bottomDp));
        return params;
    }

    public static LinearLayout.LayoutParams getParams() {
        return new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
    }

    public static void setMarginByDeviceSize(Context context, TextView... textViews) {
        setMarginByDeviceSizeScale(context, 1.0, textViews);
    }

    public static void setMarginByDeviceSize(Context context, View view, int... resIds) {
        setMarginByDeviceSizeScale(context, view, 1.0, resIds);
    }

    public static void setMarginByDeviceSizeScale(Context context, View view, double scale, int... resIds) {
        List<TextView> textViews = new ArrayList<>();
        for (int resId : resIds)
            textViews.add((TextView) view.findViewById(resId));
        setMarginByDeviceSizeScale(context, scale, textViews.toArray(new TextView[resIds.length]));
    }

    public static ImageView createImageView(Context context, int resource, int background) {
        ImageView imageView = new ImageView(context);
        imageView.setImageResource(resource);
        imageView.setBackgroundResource(background);
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        imageView.setLayoutParams(getParams());
        return imageView;
    }

    // 주의! (... double scale, int...resIds)가 연달아 나오지 않게.. 던지는 쪽에 resIds만 던져도 구분못한다
    public static void setFontSizeByDeviceSize(Context context, View view, int... resIds) {
        setFontSizeByDeviceSizeScale(context, view, 1.0, resIds);
    }

    public static void setFontSizeByDeviceSizeScale(Context context, View view, double scale, int... resIds) {
        List<TextView> textViews = new ArrayList<>();
        for (int resId : resIds)
            textViews.add((TextView) view.findViewById(resId));
        setFontSizeByDeviceSizeScale(context, scale, textViews.toArray(new TextView[textViews.size()]));
    }


    public static void setFontSizeByDeviceSize(Context context, TextView... textViews) {
        setFontSizeByDeviceSizeScale(context, 1.0, textViews);
    }

    public static void setFontSizeByDeviceSize(Context context, TextView textView) {
        setFontSizeByDeviceSizeScale(context, 1.0, textView);
    }

    public static List<View> getChildViews(ViewGroup view) {
        List<View> results = new ArrayList<>();
        for (int index = 0; index < view.getChildCount(); index++)
            results.add(view.getChildAt(index));
        return results;
    }

    public static void setTextViewByDeviceSize(Context context, TextView textView) {
        setTextViewByDeviceSizeScale(context, 1.0, textView);
    }

    public static void setTextViewByDeviceSize(Context context, View view, int... resIds) {
        setTextViewByDeviceSizeScale(context, view, 1.0, resIds);
    }

    public static void setTextViewByDeviceSizeScale(Context context, View view, double scale, int... resIds) {
        setFontSizeByDeviceSizeScale(context, view, scale, resIds);
        setMarginByDeviceSizeScale(context, view, scale, resIds);
    }

    public static void setTextViewByDeviceSizeScale(Context context, double scale, TextView... textViews) {
        setFontSizeByDeviceSizeScale(context, scale, textViews);
        setMarginByDeviceSizeScale(context, scale, textViews);
        setPaddingByDeviceSizeScale(context, scale, textViews);
    }

    public static void setPaddingByDeviceSizeScale(Context context, double scale, TextView... textViews) {
        if (textViews.length <= 0) return;
        if (MyDevice.isLessThan(context, MyDevice.DeviceSize.W411))
            return;
        double compensateScale = 0.3; // 1.0이면 너무 크게 패딩이 잡혀서
        double ratio = MyDevice.getDeviceSize(context).size / 360.0;
        double incremental = (ratio - 1.0) * scale * compensateScale;
        ratio = 1.0 + incremental;

        for (TextView textView : textViews) {
            int paddingLeft = (int) (textView.getPaddingLeft() * ratio);
            int paddingTop = (int) (textView.getPaddingTop() * ratio);
            int paddingBottom = (int) (textView.getPaddingBottom() * ratio);
            int paddingRight = (int) (textView.getPaddingRight() * ratio);
            textView.setPadding(paddingLeft, paddingTop, paddingRight, paddingBottom);
        }
    }

    public static void setMarginByDeviceSizeScale(Context context, double scale, TextView... textViews) {
        if (textViews.length <= 0) return;
        if (MyDevice.isLessThan(context, MyDevice.DeviceSize.W411))
            return;
        double compensateScale = 0.3; // 1.0이면 너무 크게
        double ratio = MyDevice.getDeviceSize(context).size / 360.0;
        double incremental = (ratio - 1.0) * scale * compensateScale;
        ratio = 1.0 + incremental;
        // ex) 1.7 = 1.0 + 0.7 * scale

        for (TextView textView : textViews) {
            try {
                ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) textView.getLayoutParams();
                params.leftMargin = (int) (params.leftMargin * ratio);
                params.topMargin = (int) (params.topMargin * ratio);
                params.bottomMargin = (int) (params.bottomMargin * ratio);
                params.rightMargin = (int) (params.rightMargin * ratio);
                textView.setLayoutParams(params);
            } catch (ClassCastException e) {
                //
            }
        }
    }

    public static void setPaddingByDeviceSizeScale(Context context, View... views) {
        if (MyDevice.isLessThan(context, MyDevice.DeviceSize.W411))
            return;
        double compensateScale = 0.3; // 1.0이면 너무 크게 패딩이 잡혀서
        double ratio = MyDevice.getDeviceSize(context).size / 360.0;
        double incremental = (ratio - 1.0) * compensateScale;
        ratio = 1.0 + incremental;

        for (View view : views) {
            int paddingLeft = (int) (view.getPaddingLeft() * ratio);
            int paddingTop = (int) (view.getPaddingTop() * ratio);
            int paddingBottom = (int) (view.getPaddingBottom() * ratio);
            int paddingRight = (int) (view.getPaddingRight() * ratio);
            view.setPadding(paddingLeft, paddingTop, paddingRight, paddingBottom);
        }
    }

    private static float toSP(Context context, float px) {
        if (px <= 0)
            return 0;
        return px / context.getResources().getDisplayMetrics().density;
    }


    public static float toOriginalSP(Context context, float px) {
        float scale = MyDevice.getFontScale(context);
        float sp = toSP(context, px);
        if (scale == 0)
            return sp;
        else
            return sp / scale;
    }

    private static void setFontSizeByDeviceSizeScale(Context context, double scale, TextView... textViews) {
        if (textViews.length <= 0) {
            //Log.w("DEBUG_JS", String.format("[MyView.setFontSizeByDeviceSizeScale] textViews size == 0, %s", context.getClass().getSimpleName()));
            return;
        }

        for (TextView textView : textViews) {
            float fontSize = MyDevice.toOriginalSP(context, textView.getTextSize());
            switch (MyDevice.getDeviceSize(context)) {
                default:
                    break;
                case W411:
                    fontSize = fontSize + (float) (1.2 * scale);
                    break;
                case W600:
                    fontSize = fontSize + (float) (2 * scale);
                    break;
                case W720:
                    fontSize = fontSize + (float) (3 * scale);
                    break;
                case W800:
                    fontSize = fontSize + (float) (4 * scale);
                    break;
            }
            setFontSize(fontSize, textView);
        }
    }

    public static void setFontSize(float fontSize, TextView... textViews) {
        for (TextView textView : textViews)
            textView.setTextSize(fontSize);
    }


}
