package com.teampls.cognito.lib;

/**
 * Created by Medivh on 2016-10-04.
 */
public enum DBResultType {
    FAILED, INSERTED, UPDATED, SKIPPED, INSERT_FAILED, NOT_FOUND;

    public boolean updatedOrInserted() {
        return ((this == INSERTED) || (this == UPDATED));
    }

}
