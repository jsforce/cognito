package com.teampls.cognito.lib;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.teampls.cognito.R;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Medivh on 2018-12-12.
 */

public class MySpinnerAdapter extends ArrayAdapter<String> {
    private Context context;
    private List<String> items = new ArrayList<>();
    private Set<View> resizedViews = new HashSet<>();
    private static final int textViewResourceId = R.layout.row_spinner;
    private boolean customFontEnabled = false;
    private int fontSize = 14;
    private ColorType fontColor = ColorType.Black;

    public MySpinnerAdapter(final Context context, final List<String> objects) {
        super(context, textViewResourceId, objects);
        this.items = objects;
        this.context = context;
        setDropDownViewResource(textViewResourceId); // checked textview
        //setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // checked textview
    }

    public void setFont(int size, ColorType color) {
        this.fontSize = size;
        this.fontColor = color;
        this.customFontEnabled = true;
    }

    // 드랍다운뷰
    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(context);
            convertView = inflater.inflate(textViewResourceId , parent, false);
            //convertView = inflater.inflate(android.R.layout.simple_spinner_item, parent, false); // textview
        }

        TextView tv = (TextView) convertView.findViewById(R.id.spinner_text);
        tv.setText(items.get(position));
        if (customFontEnabled) {
            tv.setTextSize(fontSize);
            switch (fontColor) {
                default:
                    tv.setTextColor(fontColor.colorInt);
                    break;
                case White:
                    tv.setTextColor(ColorType.Black.colorInt); // 바탕이 회색이다
                    break;
            }
        }
 //       if (resizedViews.contains(tv) == false) {
            MyView.setFontSizeByDeviceSize(context, tv);
            resizedViews.add(tv);
  //      }
        return convertView;
    }

    // 기본 뷰
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(context);
            convertView = inflater.inflate(textViewResourceId, parent, false);
        }
        TextView tv = (TextView) convertView.findViewById(R.id.spinner_text);
        tv.setText(items.get(position));
        if (customFontEnabled) {
            tv.setTextSize(fontSize);
            tv.setTextColor(fontColor.colorInt);
        }
        if (resizedViews.contains(tv) == false) {
            MyView.setFontSizeByDeviceSize(context, tv);
            MyView.setMargin(context, tv, 0, 0, 0, 0);
        }
        return convertView;
    }
}