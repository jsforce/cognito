package com.teampls.cognito.lib;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.fragment.app.FragmentActivity;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Medivh on 2016-10-24.
 */
abstract public class BaseActivity extends FragmentActivity implements View.OnClickListener {
    protected Context context = BaseActivity.this;
    protected MyDB myDB;
    private Set<View> resizedTexts = new HashSet<>();
    private Set<ProgressDialog> progressDialogs = new HashSet<>(); // 삭제될때 같이 없애기 위해
    protected MyActionbar myActionBar;

    public void addProgressDialog(ProgressDialog progressDialog) {
        if (progressDialog != null)
            progressDialogs.add(progressDialog);
    }

    @Override
    protected void onDestroy() {
        for (ProgressDialog progressDialog : progressDialogs)
            if (progressDialog != null) {
                Log.i("DEBUG_JS", String.format("[BaseActivity.onDestroy] dismiss %s", progressDialog.getClass().getSimpleName()));
                progressDialog.dismiss();
            }
        super.onDestroy();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i("DEBUG_JS", String.format("==%s==", getClass().getSimpleName()));
        super.onCreate(savedInstanceState);
        setContentView(getThisView());
        myDB = MyDB.getInstance(context);
        myActionBar = new MyActionbar();
        myActionBar.initialize(context, "", this);
    }

    @Override
    public void onClick(View view) {
        if (view.getTag() instanceof MyMenu)
            onMyMenuSelected(((MyMenu) view.getTag()));
    }


    public void setBackground(Pair<String, String> hexs) {
        if (myActionBar == null) return;
        if (hexs.first.isEmpty() == false)
            myActionBar.setBackground(hexs.first);
    }

    abstract protected void onMyMenuSelected(MyMenu myMenu);

    public abstract int getThisView();

    public void doFinishForResult() {
        Intent intent = new Intent();
        setResult(Activity.RESULT_OK, intent);
        finish();
    }

    abstract public void onMyMenuCreate();

    public void setOnClick(int... resIds) {
        for (int resId : resIds) {
            setOnClick(resId);
        }
    }

    public View setOnClick(int viewId) {
        View view = findViewById(viewId);
        view.setOnClickListener(this);
        if (view instanceof TextView && resizedTexts.contains(view) == false) {
            MyView.setTextViewByDeviceSize(context, (TextView) view);
            resizedTexts.add(view);

        } else if (view instanceof LinearLayout) {
            for (View childView : MyView.getChildViews((LinearLayout) view)) {
                if (childView instanceof TextView && resizedTexts.contains(childView) == false) {
                    MyView.setTextViewByDeviceSize(context, (TextView) childView);
                    resizedTexts.add(childView);
                }
            }
        }
        return view;
    }

    public ViewGroup getView() {
        return (ViewGroup) ((ViewGroup) this.findViewById(android.R.id.content)).getChildAt(0);
    }

    public void setVisibility(boolean visible, int... resIds) {
        setVisibility(visible ? View.VISIBLE : View.GONE, resIds);
    }

    public void setVisibility(boolean visible, View... views) {
        setVisibility(visible ? View.VISIBLE : View.GONE, views);
    }

    public void setVisibility(int visibility, int... resIds) {
        for (int resId : resIds)
            findViewById(resId).setVisibility(visibility);
    }

    public void setVisibility(int visibility, View... views) {
        for (View view : views)
            view.setVisibility(visibility);
    }

    public <T extends View> T setView(int viewId) {
        return (T) findViewById(viewId);
    }

    protected void doFinish() {
    }


}

/*

 */