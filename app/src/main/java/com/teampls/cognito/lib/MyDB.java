package com.teampls.cognito.lib;

import android.content.ContentValues;
import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.teampls.cognito.database.BaseDB;

public class MyDB extends BaseDB<MyRecord> {
    private static MyDB instance = null;
    private static int Ver = 1;

    public enum Column {
        _id, username, phone_number, nickname, password, confirmed;

        public static String[] toStrs() {
            return BaseUtils.toStrs(Column.values());
        }
    }

    public static MyDB getInstance(Context context) {
        if (instance == null)
            instance = new MyDB(context);
        return instance;
    }

    private MyDB(Context context) {
        super(context, "MyDB", "MyListTable", Ver, Column.toStrs());
    }

    protected ContentValues toContentvalues(MyRecord record) {
        ContentValues contentvalues = new ContentValues();
        contentvalues.put(Column.username.toString(), record.username);
        contentvalues.put(Column.phone_number.toString(), record.phoneNumber);
        contentvalues.put(Column.nickname.toString(), record.nickname);
        contentvalues.put(Column.password.toString(), record.password);
        contentvalues.put(Column.confirmed.toString(), record.confirmed);
        return contentvalues;
    }

    public String getPrivateName() {
        if (isOpen() == false) open();
        return this.getString(Column.nickname, 1);
    }

    public String getPassword() {
        if (isOpen() == false) open();
        return this.getString(Column.password, 1);
    }

    public MyRecord getMyRecord() {
        if (registered() == false) {
            return new MyRecord();
        }
        return getRecord(1);
    }

    public String getUsername() {
        return this.getString(Column.username, 1);
    }

    @Override
    protected int getId(MyRecord record) {
        return record.id;
    }

    @Override
    protected MyRecord getEmptyRecord() {
        return new MyRecord();
    }


    @Override
    protected MyRecord createRecord(Cursor cursor) {
        return new MyRecord(cursor.getInt(Column._id.ordinal()),
                cursor.getString(Column.username.ordinal()),
                cursor.getString(Column.phone_number.ordinal()),
                cursor.getString(Column.nickname.ordinal()),
                cursor.getString(Column.password.ordinal()),
                BaseUtils.toBoolean(cursor.getInt(Column.confirmed.ordinal()))
        );
    }

    public String getUserPoolNumber() {
        String COUNTRY_CODE_KR = "+82";
        if (getPrivateNumber().isEmpty())
            return COUNTRY_CODE_KR + "";
        else
            return COUNTRY_CODE_KR + getPrivateNumber().substring(1); // 010에서 0제거
    }

    public void register(MyRecord record) {
        if (isOpen() == false) open();
        switch (this.getSize()) {
            default:
                new Exception(String.format("[%s.register] invalid size %d", getClass().getSimpleName(), this.getSize()));
                break;
            case 0:
                insert(record);
                break;
            case 1:
                update(1, record);
                break;
        }
    }

    public void resetPassword(String phoneNumber, String newPassword, boolean confirmed) {
        logOut();
        register(new MyRecord(phoneNumber, newPassword, true));
    }

    public void logOut() {
        try {
            Context otherContext = context.createPackageContext(context.getPackageName(), Context.CONTEXT_IGNORE_SECURITY);
            DBHelper otherHelper = new DBHelper(otherContext);
            SQLiteDatabase otherDatabase = otherHelper.getWritableDatabase();
            // DELETE
            otherDatabase.execSQL(MyQuery.delete(DB_TABLE));
            otherHelper.onCreate(otherDatabase);
            Log.w("DEBUG_JS", String.format("[%s] delete DB : %s", DB_NAME, otherContext.getPackageName()));
            // TODO 삭제당한 앱에서는 이를 인지하고 앱을 종료해야 한다
        } catch (PackageManager.NameNotFoundException e) {
            // not found
        }
    }

    public String getPrivateNumber() {
        if (isOpen() == false) open();
        return this.getString(Column.phone_number, 1);
    }

    public boolean registered() {
        if (isOpen() == false) open();
        return ((getSize() == 1) && (getPrivateNumber().isEmpty() == false));
    }

    public boolean confirmed() {
        if (isOpen() == false) open();
        if (registered()) {
            return BaseUtils.toBoolean(this.getInt(Column.confirmed, 1));
        } else {
            return false;
        }
    }

}
