package com.teampls.cognito.lib;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import com.teampls.cognito.aws.LoginState;
import com.teampls.cognito.database.KeyValueDB;

public class BaseAppWatcher extends Service {
    public static String appDir = "";
    private static LoginState loginState = LoginState.Default;
    public static final String loginTime = "loginTime";

    public static void setLoginState(final Context context, final LoginState loginState) {
        BaseAppWatcher.loginState = loginState;
        Log.w("DEBUG_JS", String.format("[AppWatcher.setLoginState] %s, log-in : %s", context.getClass().getSimpleName(), loginState.toString()));
        if (loginState == LoginState.onSuccess)
            KeyValueDB.getInstance(context).put(loginTime, System.currentTimeMillis());
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public static void init(Context context, boolean isProduction, String appDir) {
        BaseAppWatcher.appDir = MyDevice.teamplDir + appDir;
    }
}
