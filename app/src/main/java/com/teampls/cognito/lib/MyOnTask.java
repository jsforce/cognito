package com.teampls.cognito.lib;

/**
 * Created by Medivh on 2016-04-15.
 */
public interface MyOnTask<T> {
    void onTaskDone(T result);
}
