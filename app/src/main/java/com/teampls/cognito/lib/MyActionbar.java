package com.teampls.cognito.lib;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.teampls.cognito.R;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Medivh on 2016-05-12.
 */
public class MyActionbar {
    private int thisView = R.layout.base_bar;
    private Context context;
    private ActionBar actionBar;
    private ArrayList<MyMenu> myMenus = new ArrayList<>();
    private ArrayList<ImageView> imageViews = new ArrayList<>();
    private CheckBox checkBox0, checkBox1, checkBox2;
    private View.OnClickListener onClickListener;
    private TextView tvTitle, tvSubTitle;
    private DateTime refreshTime = Empty.dateTime;
    private LinearLayout container, customContainer;
    private View spinnerSpace;
    private View bottomLine;
    private ImageView ivQuick;
    private Spinner spinner;
    protected MySpinnerAdapter spinnerAdapter;

    public ActionBar getActionBar() {
        return actionBar;
    }

    public void initialize(final Context context, String title, View.OnClickListener onClickListener) {
        this.context = context;
        this.onClickListener = onClickListener;

        // custom actionbar setting
        LayoutInflater inflate = LayoutInflater.from(context);
        actionBar = ((Activity) context).getActionBar();
        if (actionBar == null) {
            Log.e("DEBUG_JS", String.format("[MyActionbar.initBase] actionBar == null"));
            return;
        }
        actionBar.setDisplayShowHomeEnabled(false);
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);

        ViewGroup view = (ViewGroup) inflate.inflate(thisView, null);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setCustomView(view); // 수동 생성

        ivQuick = view.findViewById(R.id.actionbar_quick);
        ivQuick.setVisibility(View.VISIBLE);

        tvTitle = view.findViewById(R.id.actionbar_title);
        if (title.isEmpty()) {
            MyUI.setVisibility(context, tvTitle, View.GONE);
        } else {
            setTitle(title);
        }
        tvSubTitle = view.findViewById(R.id.actionbar_subTitle);

        // 바의 크기는 내용물의 visibility와 상관없이 일정하게 유지해야 함
        container = view.findViewById(R.id.actionbar_container);
        container.getLayoutParams().width = MyDevice.getWidth(context);
        customContainer = view.findViewById(R.id.actionbar_custom_container);
        spinnerSpace = view.findViewById(R.id.actionbar_spinner_space);
        bottomLine = view.findViewById(R.id.actionbar_bottomLine);
        bottomLine.setVisibility(View.GONE);

        imageViews.add((ImageView) view.findViewById(R.id.actionbar_menu1));
        imageViews.add((ImageView) view.findViewById(R.id.actionbar_menu2));
        imageViews.add((ImageView) view.findViewById(R.id.actionbar_menu3));
        imageViews.add((ImageView) view.findViewById(R.id.actionbar_menu4));
        imageViews.add((ImageView) view.findViewById(R.id.actionbar_menu5));
        imageViews.add((ImageView) view.findViewById(R.id.actionbar_menu6));
        imageViews.add((ImageView) view.findViewById(R.id.actionbar_menu7));
        for (ImageView imageView : imageViews) {
            imageView.setOnClickListener(onClickListener);
            imageView.setVisibility(View.GONE);
        }
        checkBox0 = view.findViewById(R.id.actionbar_check0);
        checkBox1 = view.findViewById(R.id.actionbar_check1);
        checkBox2 = view.findViewById(R.id.actionbar_check2);
        spinner = view.findViewById(R.id.actionbar_spinner);
        hideRefreshTime();
    }

    public Spinner getSpinner() {
        return spinner;
    }

    public void setSpinner(List<String> strings, String initString, final MyOnClick<String> myOnClick) {
        spinnerAdapter = new MySpinnerAdapter(context, strings);
        spinnerAdapter.setFont(18, ColorType.White);
        spinner.setVisibility(View.VISIBLE);
        spinner.setAdapter(spinnerAdapter);
        spinnerSpace.setVisibility(View.VISIBLE);
        spinner.setOnItemSelectedListener(new Spinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                String str = spinnerAdapter.getItem(position);
                if (myOnClick != null)
                    myOnClick.onMyClick(view, str);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        int position = 0;
        for (String str : strings) {
            if (str.equals(initString)) {
                spinner.setSelection(position);
                break;
            }
            position++;
        }
    }

    // NOT-USED
    private void addToCustomView(View view) {
        customContainer.addView(view);
    }

    public CheckBox getCheckBox(int index) {
        switch (index) {
            default:
            case 0:
                return checkBox0;
            case 1:
                return checkBox1;
            case 2:
                return checkBox2;
        }
    }

    public CheckBox setCheckBox(int index, String title, boolean value, View.OnClickListener onCheckBoxClick) {
        CheckBox checkBox;
        switch (index) {
            default:
            case 0:
                checkBox = checkBox0;
                break;
            case 1:
                checkBox = checkBox1;
                break;
            case 2:
                checkBox = checkBox2;
                break;
        }
        checkBox.setText(title);
        checkBox.setChecked(value);
        checkBox.setVisibility(View.VISIBLE);
        checkBox.setOnClickListener(onCheckBoxClick);
        MyView.setTextViewByDeviceSize(context, checkBox);
        return checkBox;
    }

    ;

    public void setBackground(String hex) {
        container.setBackgroundColor(ColorType.toInt(hex));
    }

    public void getBackroundColor() {
    }

    public void setBottomLine(String hex) {
        bottomLine.setVisibility(View.VISIBLE);
        bottomLine.setBackgroundColor(ColorType.toInt(hex));
    }

    public void setRefreshTime() {
        refreshTime = DateTime.now();
        tvSubTitle.setText(String.format("%s 자료", refreshTime.toString(BaseUtils.HS_Kor_Format)));
    }

    public void setSubTitle(String subTitle) {
        tvSubTitle.setVisibility(View.VISIBLE);
        tvSubTitle.setText(subTitle);
    }

    public void setSubTitle(final String subTitle, final boolean doCenterGravity) {
        if (MyUI.isActiveActivity(context, "MyAsyncTask") == false)
            return;

        ((Activity) context).runOnUiThread(new Runnable() {
            public void run() {
                tvSubTitle.setVisibility(View.VISIBLE);
                tvSubTitle.setText(subTitle);
                if (doCenterGravity)
                    tvSubTitle.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL);
            }
        });
    }

    public void setTitleColor(int colorInt) {
        tvTitle.setTextColor(colorInt);
        tvSubTitle.setTextColor(colorInt);
    }

    public void hideRefreshTime() {
        tvSubTitle.setVisibility(View.GONE);
    }

    public void hideLauncher() {
        ivQuick.setVisibility(View.GONE);
    }

    public void hideTitles() {
        spinnerSpace.setVisibility(View.GONE);
    }

    public void showRefreshTime() {
        tvSubTitle.setVisibility(View.VISIBLE);
    }

    public TextView getTitle() {
        return tvTitle;
    }

    public TextView getSubTitle() {
        return tvSubTitle;
    }

    public MyActionbar add(boolean doAdd, MyMenu myMenu) {
        if (doAdd == false)
            return this;
        return add(myMenu);
    }

    public MyActionbar add(MyMenu myMenu) {
        myMenus.add(myMenu);
        switch (myMenu.actionEnum) {
            case MenuItem.SHOW_AS_ACTION_ALWAYS:
                if (myMenus.size() > imageViews.size()) {
                    Log.e("DEBUG_JS", String.format("[MyActionbar.addProgressDialog] max actionbar = %d", imageViews.size()));
                    return this;
                }
                int imageViewIndex = myMenus.size() - 1;
                imageViews.get(imageViewIndex).setVisibility(View.VISIBLE);
                imageViews.get(imageViewIndex).setImageResource(myMenu.iconRes);
                imageViews.get(imageViewIndex).setTag(myMenu);
                break;
            case MenuItem.SHOW_AS_ACTION_NEVER:
                break;
        }
        return this;
    }

    public void hide() {
        actionBar.hide();
    }

    public void show() {
        actionBar.show();
    }

    public void clear() {
        myMenus.clear();
        for (ImageView imageView : imageViews)
            imageView.setVisibility(View.GONE);
    }

    public void popupActionNeverMenusButton() {
        new OptionButtonDialog(context, myMenus, onClickListener).show();
    }

    public void popupActionNeverMenus() {
        new OptionGridDialog(context, myMenus, onClickListener).show();
    }


    public void setTitle(final String title) {
        setTitle(title, false);
    }

    public void setTitleOnClick(View.OnClickListener onClick) {
        if (tvTitle != null)
            tvTitle.setOnClickListener(onClick);
    }

    public void setTitle(final String title, final boolean doCenterGravity) {
        if (MyUI.isActiveActivity(context, "MyAsyncTask") == false)
            return;

        ((Activity) context).runOnUiThread(new Runnable() {
            public void run() {
                tvTitle.setVisibility(View.VISIBLE);
                tvTitle.setText(title);
                if (doCenterGravity)
                    tvTitle.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL);
            }
        });
    }

    class OptionGridDialog extends Dialog implements View.OnClickListener {
        private int thisView = R.layout.dialog_option_grid;
        private GridLayout container;
        private View.OnClickListener onClickListener;

        public OptionGridDialog(Context context, ArrayList<MyMenu> myMenus, View.OnClickListener onClickListener) {
            super(context);
            this.onClickListener = onClickListener;
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            setContentView(thisView);
            container = (GridLayout) findViewById(R.id.dialog_option_grid_container);

            int deviceSize = MyDevice.getDeviceSize(context).size;
            for (MyMenu myMenu : myMenus) {
                if (myMenu.actionEnum == MenuItem.SHOW_AS_ACTION_NEVER) {
                    ImageView imageView = MyView.createImageView(context, myMenu.iconRes, R.color.White);
                    imageView.setOnClickListener(this);
                    imageView.setTag(myMenu);
                    if (deviceSize < 411) {
                        container.addView(imageView, MyView.getGridParamsWithMargin(context, 70, 70, 3, 3, 3, 3)); // 411이하
                    } else if (deviceSize < 600) {
                        container.addView(imageView, MyView.getGridParamsWithMargin(context, 75, 75, 3, 3, 3, 3)); // 411이상
                    } else if (deviceSize < 720) {
                        container.addView(imageView, MyView.getGridParamsWithMargin(context, 80, 80, 3, 3, 3, 3)); // 600이상
                    } else {
                        container.addView(imageView, MyView.getGridParamsWithMargin(context, 100, 100, 3, 3, 3, 3)); // 720이상
                    }
                }
            }
        }

        @Override
        public void onClick(View view) {
            onClickListener.onClick(view);
            dismiss();
        }

    }

    class OptionButtonDialog extends Dialog implements View.OnClickListener {
        private int thisView = R.layout.dialog_option_button;
        private LinearLayout optionLayout;
        private View.OnClickListener onClickListener;

        public OptionButtonDialog(Context context, ArrayList<MyMenu> myMenus, View.OnClickListener onClickListener) {
            super(context);
            this.onClickListener = onClickListener;
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            setContentView(thisView);
            optionLayout = (LinearLayout) findViewById(R.id.dialog_simpleOption_layout);

            LinearLayout.LayoutParams params = MyView.getParamsWithMargin(context, MyView.PARENT, MyView.CONTENT, 0, 5, 0, 0);
            for (MyMenu myMenu : myMenus) {
                if (myMenu.actionEnum == MenuItem.SHOW_AS_ACTION_NEVER) {
                    Button button = MyView.createButton(context, myMenu.korStr, 18, 15);
                    button.setOnClickListener(this);
                    optionLayout.addView(button, params);
                }
            }
            Button button = MyView.createButton(context, "닫         기", 18, 15);
            button.setOnClickListener(this);
            optionLayout.addView(button, params);
        }

        @Override
        public void onClick(View view) {
            onClickListener.onClick(view);
            dismiss();
        }

    }
}



