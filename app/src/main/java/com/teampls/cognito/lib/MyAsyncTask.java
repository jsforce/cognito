package com.teampls.cognito.lib;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

/**
 * Created by Medivh on 2016-09-26.
 */
public class MyAsyncTask {
    private Context context;
    private String location = "";
    protected ProgressDialog progressDialog;

    public MyAsyncTask(final Context context) {
        this(context, "");
    }

    public MyAsyncTask(Context context, final String callLocation, final String progressMessage) {
        this(context, callLocation);
        if (TextUtils.isEmpty(progressMessage) == false && MyUI.isActiveActivity(context, callLocation)) {
            progressDialog = new ProgressDialog(context);
            progressDialog.setMessage(progressMessage);
            progressDialog.show();
        }
    }

    public ProgressDialog getProgressDialog() {
        return progressDialog;
    }

    protected void setProgressMessage(String message) {
        if (progressDialog == null)
            return;
        MyUI.setMessage(context, progressDialog, message);
    }

    private void dismissProgress() {
        if (progressDialog != null && MyUI.isActiveActivity(context,"dismissProgress")) { // activity가 먼저 죽는 경우가 있음
            progressDialog.dismiss();
        }
    }

    public MyAsyncTask(Context context, final String location) {
        this.context = context;
        this.location = location;

        ((Activity) context).runOnUiThread(new Runnable() {
            public void run() {
                onPreExecute();
                execute();
            }
        });
    }

    public void onPreExecute() {

    }

    public void doInBackground() {
    }

    private void execute() {
        new Thread() {
            public void run() {
                try {
                    doInBackground();
                    ((Activity) context).runOnUiThread(new Runnable() {
                        public void run() {
                            dismissProgress();
                            onPostExecutionUI();
                        }
                    });
                } catch (final Exception e) {
                    ((Activity) context).runOnUiThread(new Runnable() {
                        public void run() {
                            dismissProgress();
                            catchException(e);
                        }
                    });
                }
            }
        }.start();
    }

    public void onPostExecutionUI() {
    }

    public void catchException(Exception e) {
        Log.e("DEBUG_JS", String.format("[MyAsyncTask.run] %s.%s : %s", context.getClass().getSimpleName(), location, e.getLocalizedMessage()));
    }
}
