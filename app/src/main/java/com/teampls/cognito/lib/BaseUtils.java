package com.teampls.cognito.lib;

import android.text.TextUtils;
import android.util.Log;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.regex.Pattern;

public class BaseUtils {
    static final public DateTimeFormatter HS_Kor_Format = DateTimeFormat.forPattern("H시m분");
    static final public DateTimeFormatter fullFormat = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");

    private static Random random = new Random();
    static final private String alphabetLower = "abcdefghijklmnopqrstuvwxyz";
    static final private String numbers = "1234567890";

    public static int toInt(String str, int defaultValue) {
        if (TextUtils.isEmpty(str))
            return defaultValue;
        str = str.replace(",", "");
        try {
            if (isInteger(str)) {
                return Integer.valueOf(str);
            } else {
                return defaultValue;
            }
        } catch (NumberFormatException e) {
            return defaultValue;
        }
    }

    public static DateTime toDateTime(String dateTimeStr) {
        return toDateTime(dateTimeStr, false);
    }

    private static DateTime toDateTime(String dateTimeStr, boolean doIgnoreError) {
        try {
            if (dateTimeStr == null) {
                return Empty.dateTime;
            } else if (dateTimeStr.isEmpty()) {
                return Empty.dateTime;
            } else if (dateTimeStr.contains("T")) {
                return DateTime.parse(dateTimeStr);
            } else {
                return fullFormat.parseDateTime(dateTimeStr);
            }

        } catch (IllegalArgumentException e) {
            if (doIgnoreError == false)
                Log.e("DEBUG_JS", String.format("[toDateTime] invalid dateTime format : %s", dateTimeStr));
            return Empty.dateTime;
        }
    }



    public static boolean isFloat(String str) {
        if (str == null) return false;
        Pattern floatPattern = Pattern.compile("[+-]?\\d+\\.?\\d+");
        Pattern integerPattern = Pattern.compile("[+-]?\\d+");
        return floatPattern.matcher(str).matches() || integerPattern.matcher(str).matches();
    }

    public static int toInt(boolean value) {
        return ((value) ? 1 : 0);
    }

    public static String toDbStr(boolean value) {
        return Integer.toString(toInt(value));
    }

    public static <T> List<String> toStringList(List<T> values) {
        if (values.size() == 0)
            return new ArrayList<>();

        List<String> results = new ArrayList<>();
        Object value0 = values.get(0);
        if (value0 instanceof Integer) {
            for (T value : values)
                results.add(Integer.toString((Integer) value));
        } else if (value0 instanceof Boolean) {
            for (T value : values)
                results.add(BaseUtils.toDbStr((Boolean) value));
        } else if (value0 instanceof String) {
            for (T value : values)
                results.add(String.valueOf(value));
        } else if (value0 instanceof Double || value0 instanceof Float) {
            for (T value : values)
                results.add(Double.toString((Double) value));
        } else {
            for (T value : values)
                results.add(value.toString());
            Log.e("DEBUG_JS", String.format("[BaseUtils.toStringList] not defined data type : %s", value0.getClass().getSimpleName()));
        }
        return results;
    }

    public static String getRandomPassword(int len) {
        StringBuilder string = new StringBuilder(len);
        int position = random.nextInt(len);
        for (int i = 0; i < len; i++) {
            if (i == position) {
                string.append(numbers.charAt(random.nextInt(numbers.length())));
            } else {
                string.append(alphabetLower.charAt(random.nextInt(alphabetLower.length())));
            }
        }
        return string.toString().trim();
    }

    public static String toSHA256(String str) {
        String SHA = "";
        try {
            MessageDigest sh = MessageDigest.getInstance("SHA-256");
            sh.update(str.getBytes());
            byte byteData[] = sh.digest();
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < byteData.length; i++) {
                sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
            }
            SHA = sb.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            SHA = null;
        }
        return SHA;
    }

    public static String encode(String str, String phoneNumber) {
        //    Log.i("DEBUG_JS", String.format("[BaseUtils.encode] %s, %s", str, phoneNumber));
        String result = toSHA256(str);
        if (result.length() >= 10)
            result = result.substring(0, 10);
        if (phoneNumber.isEmpty())
            phoneNumber = "0";
        result = "s" + result + phoneNumber.substring(phoneNumber.length() - 1);
        //    Log.i("DEBUG_JS", String.format("[BaseUtils.encode] result = %s", result));
        return result;
    }

    public static boolean toBoolean(int value) throws IllegalArgumentException {
        if ((Integer) value == null) {
            Log.e("DEBUG_JS", String.format("[BaseUtils.toBoolean] null value is found"));
            return false;
        }
        if ((value == 0) || (value == 1)) {
            return (value == 1);
        } else {
            throw new IllegalArgumentException(String.format("[toBoolean] %d is not boolean", value));
        }
    }

    public static String[] toStrs(Enum[] values) {
        String[] columns = new String[values.length];
        for (int i = 0; i < values.length; i++) {//
            columns[i] = values[i].toString();
        }
        return columns;
    }

    public static String toPhoneFormat(String phoneNumber) {
        // +82-10-1234-5678, 010-1234-5678, 01012345678, _, 02-1234-5678, fdsf, .....다양한 입력 형태에 대응
        if (phoneNumber == null) return "";
        if (phoneNumber.isEmpty()) return "";
        if (isValidPhoneNumber(toSimpleForm(phoneNumber)) == false)
            return phoneNumber;
        phoneNumber = toSimpleForm(phoneNumber);
        if (phoneNumber.length() == 10) {
            return String.format("%s-%s-%s", phoneNumber.substring(0, 3), phoneNumber.substring(3, 6), phoneNumber.substring(7, phoneNumber.length()));
        } else if (phoneNumber.length() == 11) {
            return String.format("%s-%s-%s", phoneNumber.substring(0, 3), phoneNumber.substring(3, 7), phoneNumber.substring(7, phoneNumber.length()));
        } else {
            return phoneNumber;
        }
    }

    public static boolean isValidPhoneNumber(String phoneNumber) {
        phoneNumber = toSimpleForm(phoneNumber);
        if (isInteger(phoneNumber) == false) return false;  // 모두 숫자?
        switch (phoneNumber.length()) {     // 자리수는 10 또는 11
            case 10:
            case 11:
                break;
            default:
                return false;
        }

        List<String> validHeaders = BaseUtils.toList("010", "011", "016", "017", "018", "019");
        return (validHeaders.contains(phoneNumber.substring(0, 3)));
    }

    public static boolean isInteger(String str) {
        if (str == null) return false;
        Pattern integerPattern = Pattern.compile("[+-]?\\d+");
        return integerPattern.matcher(str).matches();
    }

    public static String toSimpleForm(String phoneFormat) {
        return phoneFormat.replace("+82", "0").replace(" ", "").replace("-", "");
    }

    public static List<String> toList(String... strings) {
        List<String> results = new ArrayList<>();
        for (String string : strings)
            if (string != null)
                results.add(string);
        return results;
    }

    public static <T> List<T> toList(T... records) {
        List<T> results = new ArrayList<>();
        for (T record : records)
            results.add(record);
        return results;
    }
}
