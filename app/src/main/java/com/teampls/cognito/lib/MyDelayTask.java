package com.teampls.cognito.lib;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Medivh on 2016-04-20.
 */
abstract public class MyDelayTask {
    private ProgressDialog progressDialog;

    public MyDelayTask(final Context context, long delay_ms) {
        this(context, delay_ms, "");
    }


    public MyDelayTask(final Context context, long delay_ms, String message) {
        if (message.isEmpty() == false) {
            progressDialog = new ProgressDialog(context);
            progressDialog.setMessage(message);
            progressDialog.show();
        }

        new Timer().schedule(new TimerTask() {
            public void run() {

                if (MyUI.isActivity(context, "MyAsyncTask") == false)
                    return;

                ((Activity) context).runOnUiThread(new Runnable() {
                    public void run() {
                        if (progressDialog != null)
                            progressDialog.dismiss();
                        onFinish();
                    }
                });
            }
        }, delay_ms);
        return;
    }

    abstract public void onFinish();
}
