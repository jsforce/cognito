package com.teampls.cognito.lib;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

public class MyUI {

    public static void setVisibility(final Context context, final View view, final int visibility) {
        if (isActiveActivity(context, "setVisibilty") == false) return;
        ((Activity) context).runOnUiThread(new Runnable() {
            public void run() {
                view.setVisibility(visibility);
            }
        });
    }

    public static void setMessage(final Context context, final ProgressDialog progressDialog, final String message) {
        if (isActiveActivity(context, "progressDialog") == false) return;
        ((Activity) context).runOnUiThread(new Runnable() {
            public void run() {
                if (progressDialog == null) return;
                progressDialog.setMessage(message);
            }
        });
    }

    public static boolean isActivity(Context context, String message) {
        if (context == null) {
            Log.w("DEBUG_JS", String.format("[MyUI.isActivity] context == null : %s", message));
            return false;
        }

        if (context instanceof Activity == false) {
            Log.w("DEBUG_JS", String.format("[MyUI.isActivity] context = NOT activity : %s", message));
            return false;
        }
        return true;
    }

    public static void toast(final Context context, final String message) {
        if (isActiveActivity(context, message) == false) return;
        ((Activity) context).runOnUiThread(new Runnable() {
            public void run() {
                if (context == null || message == null) return;
                Toast.makeText(context, message, Toast.LENGTH_LONG).show();
            }
        });
    }

    // SHORT보다 짧게 보여주려면 별도의 방법을 써야 한다
    public static void toastSHORT(Context context, final String message) {

        final Context finalContext = context;
        ((Activity) context).runOnUiThread(new Runnable() {
            public void run() {
                if (finalContext == null) return;
                Toast.makeText(finalContext, message, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public static boolean isActiveActivity(Context context, String message) {
        if (context == null) {
            Log.w("DEBUG_JS", String.format("[MyUI.isActiveActivity] context == null : %s", message));
            return false;
        }

        if (context instanceof Activity == false) {
            Log.w("DEBUG_JS", String.format("[MyUI.isActiveActivity] context = NOT activity : %s", message));
            return false;
        }

        if (((Activity) context).isFinishing()) {
            Log.w("DEBUG_JS", String.format("[MyUI.isActiveActivity] %s not running : %s", context.getClass().getSimpleName(), message));
            return false;
        }

        return true;
    }
}
