package com.teampls.cognito.lib;

import android.app.Dialog;
import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Medivh on 2016-09-18.
 */
abstract public class BaseDialog extends Dialog implements View.OnClickListener {
    protected Context context;
    private View dialogView;
    protected MyDB myDB;
    private Set<View> resizedViews = new HashSet<>();

    public BaseDialog(Context context) {
        super(context);
        this.context = context;
        myDB = MyDB.getInstance(context);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(getThisView());
        onCreate();
    }

    public BaseDialog(Context context, int themeResId) {
        super(context, themeResId);
        this.context = context;
        setContentView(getThisView());
        onCreate();
    }

    public void setDialogSize(int containerResId) {
        View container = findViewById(containerResId);
        ViewGroup.LayoutParams params = container.getLayoutParams();
        params.width = (int) (MyDevice.getWidth(context) * 0.95);
        params.height = (int) (MyDevice.getHeight(context) * 0.95);
        container.setLayoutParams(params);
    }

    public void setSize(double widthRatio, double heightRatio) {
        View container = getView();
        ViewGroup.LayoutParams params = container.getLayoutParams();
        if (widthRatio > 0)
            params.width = (int) (MyDevice.getWidth(context) * widthRatio);
        if (heightRatio > 0)
            params.height = (int) (MyDevice.getHeight(context) * heightRatio);
        container.setLayoutParams(params);
    }

    public void setDialogSize(boolean width, boolean height) {
        setDialogSize(width ? 0.95 : -1, height ? 0.95 : -1);
    }

    public void setDialogWidth(double widthRatio, double tabletWidthRatio) {
        if (MyDevice.isTablet(context)) {
            setDialogSize(tabletWidthRatio, -1);
        } else {
            setDialogSize(widthRatio, -1);
        }
    }

    public void setDialogHeight(double heightRatio, double tabletHeightRatio) {
        if (MyDevice.isTablet(context)) {
            setDialogSize(-1, tabletHeightRatio);
        } else {
            setDialogSize(-1, heightRatio);
        }
    }

    public void setDialogSize(double widthRatio, double heightRatio) {
        View container = getView();
        ViewGroup.LayoutParams params = container.getLayoutParams();
        if (widthRatio > 0)
            params.width = (int) (MyDevice.getWidth(context) * widthRatio);
        if (heightRatio > 0)
            params.height = (int) (MyDevice.getHeight(context) * heightRatio);
        container.setLayoutParams(params);
    }

    public void onCreate() {
        setDialogWidth(0.9, 0.7);
    }

    abstract public int getThisView();

    public void setOnClick(int... viewIds) {
        for (int viewId : viewIds)
            setOnClick(viewId);
    }

    public View setOnClick(int viewId) {
        View view = getView().findViewById(viewId);
        if (view == null) {
            Log.e("DEBUG_JS", String.format("[BaseDialog.setOnClick] view == null %d", viewId));
            return new View(context);
        }
        view.setOnClickListener(this);
        if (view instanceof Button || view instanceof TextView)
            resizeTextView((TextView) view);
        return view;
    }

    public TextView findTextViewById(int viewId, String text, boolean autoSize) {
        TextView textView = ((TextView) getView().findViewById(viewId));
        if (text != null) {
            textView.setText(text);
            if (autoSize)
                resizeTextView(textView);
        }
        return textView;
    }

    private void resizeTextView(TextView textView) {
        if (resizedViews.contains(textView) == false) {
            MyView.setTextViewByDeviceSize(context, textView);
            resizedViews.add(textView);
        }
    }

    public TextView findTextViewById(int viewId, String text) {
        return findTextViewById(viewId, text, true);
    }

    public void findTextViewByIds(int... viewIds) {
        for (int viewId : viewIds)
            findTextViewById(viewId);
    }

    public TextView findTextViewById(int viewId) {
        TextView result = findViewById(viewId);
        resizeTextView(result);
        return result;
    }

    public Button findButtonById(int viewId) {
        Button result = findViewById(viewId);
        setOnClick(viewId);
        resizeTextView(result);
        return result;
    }

    public EditText findEditTextById(int viewId, String text) {
        EditText result = findViewById(viewId);
        result.setText(text);
        result.setSelection(text.length());
        resizeTextView(result);
        return result;
    }

    public TextView setHint(int viewId, String text) {
        TextView textView = ((TextView) getView().findViewById(viewId));
        if (text != null)
            textView.setHint(text);
        return textView;
    }

    public TextView findTextViewById(int viewId, String text, String textForEmpty) {
        TextView textView = ((TextView) getView().findViewById(viewId));
        text = text.trim();
        if (text.isEmpty()) {
            textView.setTextColor(ColorType.Gray.colorInt);
            textView.setText(textForEmpty);
        } else {
            textView.setText(text);
        }
        resizeTextView(textView);
        return textView;
    }

    public ViewGroup getView() {
        return (ViewGroup) ((ViewGroup) this.findViewById(android.R.id.content)).getChildAt(0);
    }

    public void setVisibility(int visibility, int... viewIds) {
        if (dialogView == null)
            dialogView = getView();
        if (dialogView == null)
            Log.e("DEBUG_JS", String.format("[BaseDialog.setVisibility] dialogView == null"));
        for (int resId : viewIds)
            dialogView.findViewById(resId).setVisibility(visibility);
    }


    public void setVisibility(int visibility, View... views) {
        for (View view : views)
            view.setVisibility(visibility);
    }

    public void setEnabled(boolean enabled, int... viewIds) {
        if (dialogView == null)
            dialogView = getView();
        for (int resId : viewIds)
            dialogView.findViewById(resId).setEnabled(enabled);
    }

    protected void doDismiss() {
        this.dismiss();
    }

}
