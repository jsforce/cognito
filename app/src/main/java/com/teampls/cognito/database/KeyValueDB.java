package com.teampls.cognito.database;

/**
 * Created by Medivh on 2016-04-12.
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.text.TextUtils;
import android.util.Log;

import com.teampls.cognito.lib.BaseUtils;
import com.teampls.cognito.lib.DBResult;
import com.teampls.cognito.lib.Empty;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

// 실수를 방지하기 위해 소문자로 변환해 저장하는데
// 대문자로 저장했다가 그 대문자를 비교해서 처리하는 곳에서는
// 오류를 일으킬 수 있다

public class KeyValueDB extends BaseDB<KeyValueRecord> {
    private static KeyValueDB instance = null;
    private static int Ver = 1;

    public enum Column {
        _id, key, value;

        public static String[] toStrs() {
            return BaseUtils.toStrs(Column.values());
        }
    }

    private KeyValueDB(Context context) {
        super(context, "KeyValueDB", Ver, Column.toStrs());
    }

    public KeyValueDB(Context context, String DBname, int DBversion, String[] columns) {
        super(context, DBname, DBversion, columns);
    }

    public KeyValueDB(Context context, String DBname, int DBversion) {
        this(context, DBname, DBversion, Column.toStrs());
    }

    @Override
    protected int getId(KeyValueRecord record) {
        return record.id;
    }

    public static KeyValueDB getInstance(Context context) {
        if (instance == null)
            instance = new KeyValueDB(context);
        return instance;
    }

    protected ContentValues toContentvalues(KeyValueRecord record) {
        ContentValues contentvalues = new ContentValues();
        contentvalues.put(Column.key.toString(), record.key);
        contentvalues.put(Column.value.toString(), record.value);
        return contentvalues;
    }

    protected void toLogCat(int id, KeyValueRecord record, int option, String location) {
        Log.i("DEBUG_JS", String.format("[%s] [%d] key = %s, value = %s,", location,
                id, record.key, record.value));
    }

    public void put(int key, int value) {
        put(Integer.toString(key), Integer.toString(value));
    }

    public boolean put(String key, String value) {
        DBResult result;
        key = key.toLowerCase();

        if (hasKey(key)) {
            KeyValueRecord record = getRecord(getId(Column.key, key));
            record.value = value;
            result = update(record.id, record);
        } else {
            result = insert(new KeyValueRecord(0, key, value)); // key = lowercase
        }
        return result.resultType.updatedOrInserted();
    }

    public boolean put(int key, boolean value) {
        return put(Integer.toString(key), value);
    }

    public boolean put(String key, int value) {
        return put(key, String.valueOf(value));
    }

    public boolean put(String key, long value) {
        return put(key, String.valueOf(value));
    }

    public boolean put(String key, boolean value) {
        if (key.isEmpty())
            return false;
        return put(key, String.valueOf(value));
    }

    public boolean put(String key, double value) {
        return put(key, String.valueOf(value));
    }

    public <T> boolean put(String key, List<T> values) {
        return put(key, TextUtils.join(multiItemDelimiter, BaseUtils.toStringList(values)));
    }

    public List<String> getStrings(String key, String defaultString) {
        String value = getValue(key, defaultString);
        return BaseUtils.toList(value.split(multiItemDelimiter));
    }

    public boolean hasKey(String key) {
        if (key.isEmpty())
            return false;
        key = key.toLowerCase();
        return hasValue(Column.key, key);
    }

    public String getValue(String key) {
        return getValue(key, "");
    }

    public String getValue(String key, String defaultValue) {
        key = key.toLowerCase();
        if (hasKey(key)) {
            int id = getId(Column.key, key);
            return this.getRecord(id).value;
        } else {
            return defaultValue;
        }
    }

    public KeyValueRecord getRecordByKey(int key) {
        return getRecord(Column.key, Integer.toString(key));
    }

    public KeyValueRecord getRecordByKey(String key) {
        return getRecord(Column.key, key);
    }

    public int getInt(String key, int defaultValue) {
        if (hasKey(key)) {
            if (BaseUtils.isInteger(getValue(key))) {
                try {
                    return Integer.valueOf(getValue(key));
                } catch (NumberFormatException e) {
                    return defaultValue;
                }
            } else {
                return defaultValue;
            }
        } else {
            return defaultValue;
        }
    }


    public float getFloat(String key) {
        return getFloat(key, 0);
    }

    public float getFloat(String key, float defaultValue) {
        if (hasKey(key)) {
            if (BaseUtils.isFloat(getValue(key))) {
                return Float.valueOf(getValue(key));
            } else {
                return defaultValue;
            }
        } else {
            return defaultValue;
        }
    }

    public double getDouble(String key, double defaultValue) {
        if (hasKey(key)) {
            if (BaseUtils.isFloat(getValue(key))) {
                return Double.valueOf(getValue(key));
            } else {
                return defaultValue;
            }
        } else {
            return defaultValue;
        }
    }

    public int getInt(String key) {
        return getInt(key, 0);
    }

    public int getInt(int key) {
        return getInt(Integer.toString(key), 0);
    }

    public long getLong(String key) {
        return (hasKey(key) ? Long.valueOf(getValue(key)) : 0);
    }

    public long getLong(String key, long defaultValue) {
        return (hasKey(key) ? Long.valueOf(getValue(key)) : defaultValue);
    }

    public boolean getBool(int key, boolean defaultValue) {
        return getBool(Integer.toString(key), defaultValue);
    }

    public boolean getBool(String key, boolean defaultValue) {
        if (hasKey(key)) {
            return Boolean.valueOf(getValue(key));
        } else {
            return defaultValue;
        }
    }

    public boolean getBool(String key) {
        return getBool(key, false);
    }

    public DateTime getDateTime(String key) {
        if (hasKey(key)) {
            return BaseUtils.toDateTime(getValue(key));
        } else {
            return Empty.dateTime;
        }
    }

    @Override
    protected KeyValueRecord getEmptyRecord() {
        return new KeyValueRecord();
    }

    @Override
    protected KeyValueRecord createRecord(Cursor cursor) {
        return new KeyValueRecord(cursor.getInt(Column._id.ordinal()),
                cursor.getString(Column.key.ordinal()),
                cursor.getString(Column.value.ordinal())
        );
    }

    public void toLogCat(String location) {
        for (KeyValueRecord record : getRecords()) {
            toLogCat(record.id, record, 0, location);
        }
    }

    public void setIntegersAsHashSet(String key) {
        Set<String> items = new HashSet<>();
        for (String item : getValue(key).split(multiItemDelimiter))
            items.add(item);
        String result = TextUtils.join(multiItemDelimiter, items);
        //Log.i("DEBUG_JS", String.format("[KeyValueDB.addIntegers] %s, %s", key, result));
        put(key, result);
    }

    public void addIntegers(String key, List<Integer> values, int limit) {
        if (values.isEmpty()) return;
        String integersStr = getValue(key, "0");
        List<Integer> integers = new ArrayList<>();
        for (String integerStr : integersStr.split(multiItemDelimiter)) {
            int integer = BaseUtils.toInt(integerStr, -999);
            if (integer != -999)
                integers.add(integer);
        }

        for (int value : values) {
            // 중복으로 추가되는 것 방지
            if (integers.size() > 0) {
                if (value != integers.get(integers.size() - 1))
                    integers.add(value); // 추가
            } else {
                integers.add(value); // 추가
            }
        }

        if (integers.size() >= 1000)
            limit = 1000;

        if (limit > 0) {
            if (integers.size() > limit)
                integers = integers.subList(integers.size() - limit, integers.size());  // 최대 개수 지정
        }

        String result = TextUtils.join(multiItemDelimiter, integers);
//        Log.i("DEBUG_JS", String.format("[KeyValueDB.addIntegers] %s, %s", key, result));
        put(key, result);
    }

    public List<Integer> getIntegers(String key) {
        String integersStr = getValue(key, "0"); // 오래된 순서로
        //     Log.i("DEBUG_JS", String.format("[KeyValueDB.getIntegers] %s, %s", key, integersStr));
        List<Integer> integers = new ArrayList<>();
        for (String integerStr : integersStr.split(multiItemDelimiter)) {
            int integer = BaseUtils.toInt(integerStr, -999);
            if (integer != -999)
                integers.add(integer);
        }
        return integers;
    }

    public <K, V> MyMap<K, V> toMap(V defaultValue) {
        MyMap result = new MyMap(defaultValue);
        for (KeyValueRecord record : getRecords())
            result.put(record.key, record.value);
        return result;
    }

}
