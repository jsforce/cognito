package com.teampls.cognito.database;

import android.util.Log;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by Medivh on 2018-03-23.
 */

public class MyMap<K, V> extends HashMap<K, V> {
    private V defaultRecord;

    public MyMap(V defaultRecord) {
        if (defaultRecord == null)
            Log.e("DEBUG_JS", String.format("[MyMap.MyMap] defaultRecord == null"));
        this.defaultRecord = defaultRecord;
    }

    public boolean has(Object key) {
        return !(get(key) == defaultRecord);
    }

    @Override
    public V get(Object key) {
        if (defaultRecord == null)
            Log.e("DEBUG_JS", String.format("[MyMap.getSaved] defaultRecord == null"));
        V result = super.get(key);
        if (result == null) {
        //    Log.w("DEBUG_JS", String.format("[%s.getSaved] invalid key %s", getClass().getSimpleName(), key.toString()));
            return defaultRecord;
        } else {
            return result;
        }
    }

    public void find(K searchKey) {
        for (K key : keySet()) {
            Log.i("DEBUG_JS", String.format("[MyMap.find] %s(%d) %s %s(%d)", searchKey.toString(), searchKey.hashCode(), searchKey == key? "==" : "!=", key.toString(), key.hashCode()));
        }
    }

    // 없을때 새 객체를 반환, 위 함수와의 차이점은 새 객체라는 것
    public V get(Object key, V defaultRecord) {
        V result = super.get(key);
        if (result == null) {
            return defaultRecord;
        } else {
            return result;
        }
    }

    @Override
    public V remove(Object key) {
        if (containsKey(key))
            return super.remove(key);
        else
            return defaultRecord;
    }

    @Override
    public V put(K key, V value) {
//        Log.i("DEBUG_JS", String.format("[MyMap.put] %s, %s", defaultRecord.getClass().getSimpleName(), value.getClass().getSimpleName()));
        if (key == null)
            return defaultRecord;
        else if (value == null)
            return super.put(key, defaultRecord);
        else
            return super.put(key, value);
    }

    public Map<K, V> toHashMap() {
        return (HashMap) this;
    }

    public Map<K, V> toTreeMap(boolean isReverse) {
        if (isReverse) {
            TreeMap result = new TreeMap(Collections.reverseOrder());
            result.putAll(toHashMap());
            return result;
        } else
            return (TreeMap) toHashMap();
    }

    public static <K,V> MyMap createFrom(Map<K, V> map, V defaultRecord) {
        MyMap<K,V> result = new MyMap<>(defaultRecord);
        for (K key : map.keySet()) {
            if (map.get(key) != null)
                result.put(key, map.get(key));
        }
        return result;
    }

    public void addInteger(K key, Integer value) {
        if (value == null)
            return;
        Object v = (Integer) get(key) + value;
        put(key, (V) v);
    }

    public void toLogCat(String callLocation) {
        for (K key : keySet())
            Log.i("DEBUG_JS", String.format("[%s] %s, %s", callLocation, key.toString(), get(key).toString()));
    }
}
