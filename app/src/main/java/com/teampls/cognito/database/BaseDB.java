package com.teampls.cognito.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.teampls.cognito.lib.DBResult;
import com.teampls.cognito.lib.DBResultType;
import com.teampls.cognito.lib.Empty;
import com.teampls.cognito.lib.MyDevice;
import com.teampls.cognito.lib.MyQuery;
import com.teampls.cognito.lib.MyUI;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

abstract public class BaseDB<T> {
    public String DB_NAME = null;
    protected String DB_TABLE;
    protected String OLD_DB_TABLE;
    protected int DB_VERSION;
    protected String[] columns, keyColumn;
    protected String[] columnAttrs = new String[0];
    protected DBHelper helper;
    public Context context;
    protected SQLiteDatabase database = null;
    protected String KEY_ID;
    protected final int KEY_ID_Index = 0;
    protected boolean showing = false, upgraded = false;
    private boolean onTransaction = false;
    protected final String multiItemDelimiter = ",";

    public BaseDB(Context context, String DBname, String DBtable, int DBversion, String[] columns) {
        this.DB_NAME = DBname;
        this.DB_TABLE = DBtable;
        this.DB_VERSION = DBversion;
        this.columns = columns;
        KEY_ID = columns[KEY_ID_Index];
        keyColumn = new String[]{KEY_ID};

        if (context == null) {
            Log.e("DEBUG_JS", String.format("[BaseDB] context = null, DB name = %s, DB table = %s ", DBname, DBtable));
            if (this.context == null)
                Log.e("DEBUG_JS", String.format("[%s] has no valid context", getClass().getSimpleName()));
        } else {
            this.context = context;
        }

    }

    public BaseDB(Context context, String DBName, int DBversion, String[] columns) {
        this(context, DBName, DBName + "Table", DBversion, columns);
    }

    public void clear() {
        if (isOpen() == false) open();
        if (database != null)
            database.delete(DB_TABLE, null, null);
        //Log.w("DEBUG_JS", String.format("[%s.clear] %s", DB_NAME, DB_TABLE));
    }

    public void deleteDB() {
        if (isOpen() == false) open();
        try {
            database.execSQL(MyQuery.delete(DB_TABLE));
            //	database.execSQL(MyQuery.delete("old"+DB_TABLE));
            helper.onCreate(database);
            Log.w("DEBUG_JS", String.format("[%s] delete DB : %s", DB_NAME, context.getPackageName()));
        } catch (SQLException e) {
            Log.e("DEBUG_JS", String.format("[BaseDB.deleteDB]"), e);
        }
    }

    protected T getRecord(Cursor cursor) {
        if (cursor == null) {
            Log.e("DEBUG_JS", String.format("[%s.getRecordByKey] cursor = null", getClass().getSimpleName()));
            return getEmptyRecord();
        }

        if (cursor.getCount() <= 0) {
            return getEmptyRecord();
        }

        int id = cursor.getInt(Column._id.ordinal());
        if (id == 0)
            return getEmptyRecord(); // 없는 것을 조회했을때 빈 객체를 반환 (주의)

        return createRecord(cursor);
    }

    protected Cursor getCursor(int id) {
        return getCursor(columns, KEY_ID + "=" + id, null, null, null, null, "");
    }

    public T getRecord(int id) {
        if (isOpen() == false) open();
        if (id <= 0) return getEmptyRecord();
        Cursor cursor = getCursor(id);
        if (cursor == null) {
            Log.e("DEBUG_JS", String.format("[%s.getRecordByKey] cursor (id:%d) == null", getClass().getSimpleName(), id));
            return getEmptyRecord();
        }
        T record = getRecord(cursor);
        cursor.close();
        return record;
    }

    protected abstract int getId(T record);

    abstract protected T getEmptyRecord();

    abstract protected T createRecord(Cursor cursor);

    abstract protected ContentValues toContentvalues(T record);

    public String getString(Enum<?> column, int id) {
        if (isEmpty()) return Empty.string;
        if (isOpen() == false) open();
        String result = "";
        Cursor cursor = getCursor(new String[]{column.toString()}, KEY_ID + "=" + id, null, null, null, null, "getString");
        if (cursor == null) return result;
        if (cursor.getCount() <= 0) {
            cursor.close();
            return result;
        }
        result = cursor.getString(0);
        cursor.close();
        return result;
    }

    public int getInt(Enum<?> column, int id) {
        String result = getString(column, id);
        if (result.equals(Empty.string)) {
            return Empty.integer;
        } else {
            return Integer.valueOf(result);
        }
    }

    public int getSize() {
        if (isOpen() == false) open();
        return (int) DatabaseUtils.queryNumEntries(database, DB_TABLE);
    }

    public int getLastId() {
        List<Integer> ids = getIds();
        if (ids.size() == 0)
            return 0;
        return getIds().get(ids.size() - 1);
    }

    public List<Integer> getIds() {
        return getIds(null, null, null, null, null);
    }

    public List<Integer> getIds(Enum<?> column, String value) {
        return getIds(column, new String[]{value});
    }

    public List<Integer> getIds(Enum<?> column, String[] args) {
        if (column == null) {
            Log.e("DEBUG_JS", String.format("[BaseDB.getIds] %s, %s = null", DB_NAME, column.toString()));
            return new ArrayList<Integer>(0);
        }
        String selection = "";
        for (int i = 0; i < args.length; i++) {
            if (i > 0)
                selection = selection + " OR ";
            selection = selection + column.toString() + "=?";
        }
        String[] selectionArgs = args;
        return getIds(selection, selectionArgs, null, null, null);
    }

    protected boolean isPrimaryKeyDuplicated = false;
    protected Set<Integer> duplicatedIds = new HashSet<>();

    public int getId(Enum<?> candidateKey, String value) {
        List<Integer> ids = getIds(candidateKey, value);
        int result = Empty.integer;
        if (ids.size() == 1) {
            result = ids.get(0);
        } else if (ids.size() == 0) {
            // Log.w("DEBUG_JS", String.format("[%s.getId] ids.length = 0, column = %s, value = %s", DB_NAME, candidateKey.toDbString(), value));
        } else {
            isPrimaryKeyDuplicated = true;
            duplicatedIds.addAll(ids);
            Log.e("DEBUG_JS", String.format("[%s.getId] ids.length = %d, column = %s, value = %s", DB_NAME, ids.size(), candidateKey.toString(), value));
            return ids.get(ids.size() - 1);
        }
        return result;
    }

    private enum Column {_id}

    public ArrayList<Integer> getIds(String selection, String[] selectionArgs, String groupBy, String having, String orderBy) {
        return getIntegers(Column._id, selection, selectionArgs, groupBy, having, orderBy);
    }

    public boolean hasValue(Enum<?> column, String value) {
        if (isEmpty()) {
            return false;
        }
        if (column == null) {
            return false;
        }

        // return getStrings(column).contains(value); // --> 실험해보자
        return (getIds(column, new String[]{value}).size() >= 1);
    }

    private T getRecord(List<T> records, String errorMessage) {
        if (records.size() == 1) {
            return records.get(0);
        } else if (records.size() == 0) {
            return getEmptyRecord();
        } else {
            isPrimaryKeyDuplicated = true;
            for (T record : records)
                duplicatedIds.add(getId(record));
            Log.e("DEBUG_JS", String.format("[%s.getRecordByKey] count %d, %s", DB_NAME, records.size(), errorMessage));
            return records.get(records.size() - 1);
        }
    }

    public List<T> getRecords(Cursor cursor) {
        List<T> records = new ArrayList<>(0);
        if (cursor == null) return records;
        if (cursor.getCount() <= 0) {
            cursor.close();
            return records;
        }
        cursor.moveToFirst();
        do {
            records.add(getRecord(cursor));
        } while (cursor.moveToNext());
        cursor.close();
        return records;
    }

    public T getRecord(Enum<?> column, String value) {
        return getRecord(getRecords(column, value), String.format("%s, %s", column.toString(), value));
    }

    public List<T> getRecords(Enum<?> column, String value) {
        return getRecords(getCursor(column, value));
    }

    protected Cursor getCursor(Enum<?> column, String value) {
        return getCursorOrderBy(column, value, null, "", false);
    }

    protected Cursor getCursorOrderBy(Enum<?> column, String value, Enum<?> sortingColumn, String option, boolean isDescending) {
        String selection = null;
        String[] selectionArgs = null;
        if (column != null) {
            selection = column.toString() + "=?";
            selectionArgs = new String[]{value}; // selection이 ?로 지정된 경우 조건에 해당
        }

        String orderBy = null;
        if (sortingColumn != null) {
            if (isDescending) {
                orderBy = String.format("%s%s DESC", sortingColumn.toString(), option);
            } else {
                orderBy = String.format("%s%s ASC", sortingColumn.toString(), option);
            }
        }

        return getCursor(columns, selection, selectionArgs, null, null, orderBy, "");
    }

    public boolean isEmpty() {
        return (getSize() <= 0);
    }

    public List<T> getRecords() {
        return getRecords(getCursor());
    }

    protected Cursor getCursor() {
        return getCursor(null, null, null, null, null, null, "");
    }

    protected Cursor getCursor(String[] columns, String selection, String[] selectionArgs, String groupBy, String having, String orderBy, String methodStr) {
        if (isOpen() == false) open();
        if (database == null) {
            Log.e("DEBUG_JS", String.format("[%s.getCursor] database == null", getClass().getSimpleName()));
            return Empty.getCursor();
        }
        // name, columns, where, where condition, group, having, order
        try {
            Cursor cursor = database.query(DB_TABLE, columns, selection, selectionArgs, groupBy, having, orderBy);
            if (cursor == null) {
                Log.e("DEBUG_JS", String.format("[BaseDB.%s] %s count = null: selection = %s", DB_NAME, selection));
            } else if (cursor.getCount() <= 0) {
                //Log.w("DEBUG_JS",String.format("[BaseDB.%s] %s count <= 0: selection = %s",methodStr,DB_NAME,selection));
            } else {
                cursor.moveToFirst();
            }
            return cursor;
        } catch (SQLException e) {
            if (e.getLocalizedMessage().contains("no such column"))
                Log.e("DEBUG_JS", String.format("[%s.getCursor] %s", DB_NAME, e.getLocalizedMessage()));
            return Empty.getCursor();
        }
    }


    private ArrayList<Integer> getIntegers(Enum<?> interest, String selection, String[] selectionArgs, String groupBy, String having, String orderBy) {
        if (isOpen() == false) open();
        ArrayList<Integer> result = new ArrayList<Integer>(0);
        if (isEmpty()) return result;

        Cursor cursor = getCursor(new String[]{interest.toString()}, selection, selectionArgs, groupBy, having, orderBy, "getIntegers");

        if (cursor == null) return result;
        if (cursor.getCount() <= 0) {
            cursor.close();
            return result;
        }

        int inx = 0;
        for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
            result.add(cursor.getInt(0));
            inx = inx + 1;
        }

        cursor.close();
        //Log.i("DEBUG_JS",String.format("[BaseDB.getIds] %s", BaseUtils.toStr(result)));
        return result;
    }

    public DBResult insert(T record) {
        if (isOpen() == false) open();
        ContentValues contentvalues = toContentvalues(record);
        int id = (int) database.insert(DB_TABLE, null, contentvalues);
        if (id <= 0) {
            Log.e("DEBUG_JS", String.format("[%s.insert] failed id %d", getClass().getSimpleName(), getLastId() + 1));
            return new DBResult(id, DBResultType.INSERT_FAILED);
        }
        return new DBResult(id, DBResultType.INSERTED);
    }

    public DBResult update(int id, T record) {
        if (id <= 0) {
            Log.w("DEBUG_JS", String.format("[%s.updateOrInsert] id == %d", DB_NAME, id));
            return new DBResult(id, DBResultType.FAILED);
        }
        if (isOpen() == false) open();
        ContentValues contentvalues = toContentvalues(record);
        int result = database.update(DB_TABLE, contentvalues, KEY_ID + "=" + id, null);
        if (result <= 0) {
            Log.e("DEBUG_JS", String.format("[%s.updateOrInsert] failed id %d", getClass().getSimpleName(), id));
            return new DBResult(id, DBResultType.NOT_FOUND);
        }
        return new DBResult(id, DBResultType.UPDATED);
    }

    // Helper.checkInstanceAndRestart
    public void create(SQLiteDatabase db) {
        if (columnAttrs == null || columnAttrs.length != columns.length) {
            db.execSQL(MyQuery.create(DB_TABLE, columns));
        } else {
            db.execSQL(MyQuery.create(DB_TABLE, columns, columnAttrs));
        }
    }

    public void upgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w("DEBUG_JS", String.format("[%s] %s upgrade %d -> %d and deleteDB", getClass().getSimpleName(), DB_NAME, oldVersion, newVersion));
        db.execSQL(MyQuery.delete(DB_TABLE));
        helper.onCreate(db);
        upgraded = true;
    }

    public boolean isOpen() {
        if ((helper == null) || (database == null)) {
            return false;
        } else {
            return true;
        }
        //return (helper != null) || (database );
    }

    public void open() {
        try {
            helper = getDBHelper();
            database = helper.getWritableDatabase();
            if (database == null)
                Log.e("DEBUG_JS", String.format("[%s.open] database == null", DB_NAME));
        } catch (SQLException e) {
            Log.e("DEBUG_JS", String.format("[%s] fail to open %s, %d : %s", getClass().getSimpleName(), DB_TABLE, DB_VERSION, e.getLocalizedMessage()));
            MyUI.toastSHORT(context, String.format("문제가 발견되었습니다. 앱 재설치를 추천드립니다."));
        }

//        if (isColumnsValid() == false) {
//            Log.e("DEBUG_JS", String.format("[BaseDB.BaseDB] columns are invalid..."));
//            deleteDB();
//        }
    }

    public DBHelper getDBHelper() {
        return new DBHelper(context);
    }

    public String getTeamPlPath() {
        return String.format("%s/%s.db", MyDevice.teamplDir, DB_NAME);
    }

    public void close() {
        if (helper != null) helper.close();
        helper = null;
        //		Log.i("DEBUG_JS",String.format("[BaseDB.reset] %s, %s reset", DB_NAME, DB_TABLE));
    }

    public int delete(int id) {
        if (isOpen() == false) open();
        return database.delete(DB_TABLE, KEY_ID + "=" + id, null);
    }

    private boolean isLocalDB = true;
    private String customFilePath = "";

    // Helper
    public class DBHelper extends SQLiteOpenHelper {

        public DBHelper(Context context, String filePath) {
            super(context, filePath, null, DB_VERSION);
            isLocalDB = false;
            customFilePath = filePath;
        }

        public DBHelper(Context context) {
            super(context, DB_NAME, null, DB_VERSION);
            isLocalDB = true;
            customFilePath = "";
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            create(db);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            upgrade(db, oldVersion, newVersion);
        }
    }
}
