package com.teampls.cognito.database;

/**
 * Created by Medivh on 2016-04-12.
 */
public class KeyValueRecord {
    public int id = 0;
    public String key = "", value = "";

    public KeyValueRecord() {

    }

    public KeyValueRecord(int id, String key, String value) {
        this.id = id;
        this.key = key.toLowerCase();
        this.value = value;
    }
}
