package com.teampls.cognito.view_login;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.teampls.cognito.R;
import com.teampls.cognito.lib.BaseActivity;
import com.teampls.cognito.lib.BaseAppWatcher;
import com.teampls.cognito.lib.MyMenu;
import com.teampls.cognito.lib.MyView;

public class UserIsRegistered extends BaseActivity {

    public static void startActivity(Context context) {
        context.startActivity(new Intent(context, UserIsRegistered.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        myActionBar.hide();
        MyView.setTextViewByDeviceSize(context, getView(), R.id.userClass_title,
                R.id.userClass_register, R.id.userClass_login);
    }

    public void onNewUserClick(View view) {
        UserReg.startActivity(context); // 새고객 => 등록
        finish();
    }

    public void onExistingUserClick(View view) {
        UserLogIn.startActivity(context); // 기존고객 => 로그인
        finish();
    }

    @Override
    protected void onMyMenuSelected(MyMenu myMenu) {

    }

    @Override
    public int getThisView() {
        return R.layout.base_is_registered;
    }

    @Override
    public void onMyMenuCreate() {

    }
}
