package com.teampls.cognito.view_login;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoDevice;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUserSession;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.continuations.AuthenticationContinuation;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.continuations.AuthenticationDetails;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.continuations.ChallengeContinuation;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.continuations.ForgotPasswordContinuation;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.continuations.MultiFactorAuthenticationContinuation;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.handlers.AuthenticationHandler;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.handlers.ForgotPasswordHandler;
import com.teampls.cognito.R;
import com.teampls.cognito.aws.CognitoUtils;
import com.teampls.cognito.aws.MyAWSConfigs;
import com.teampls.cognito.aws.MyCognito;
import com.teampls.cognito.aws.ServiceErrorHelper;
import com.teampls.cognito.lib.BaseActivity;
import com.teampls.cognito.lib.BaseDialog;
import com.teampls.cognito.lib.BaseUtils;
import com.teampls.cognito.lib.Empty;
import com.teampls.cognito.lib.MyAlertDialog;
import com.teampls.cognito.lib.MyDelayTask;
import com.teampls.cognito.lib.MyDevice;
import com.teampls.cognito.lib.MyMenu;
import com.teampls.cognito.lib.MyRecord;
import com.teampls.cognito.lib.MyUI;
import com.teampls.cognito.view.Welcome;


/**
 * Created by Medivh on 2016-08-31.
 */
public class UserLogIn extends BaseActivity {
    protected EditText etPassword, etPhoneNumber;
    ProgressDialog progressDialog;

    public static void startActivity(Context context) {
        context.startActivity(new Intent(context, UserLogIn.class));
    }

    @Override
    public void onBackPressed() {
        UserIsRegistered.startActivity(context);
        finish();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        etPhoneNumber = (EditText) findViewById(R.id.userLogin_phonenumber);
        etPassword = (EditText) findViewById(R.id.userLogin_password);
        progressDialog = new ProgressDialog(context);

        etPhoneNumber.setText(BaseUtils.toPhoneFormat(MyDevice.getMyPhoneNumber(context)));

        setOnClick(R.id.userLogin_login, R.id.userLogin_resetPassword);
    }

    @Override
    public int getThisView() {
        return R.layout.base_user_login;
    }

    protected void onLoginClick(String phoneNumber) {
        progressDialog.setMessage("로그인 중입니다...");
        progressDialog.show();
        String userPoolPhoneNumber = CognitoUtils.convertToCompatiblePhoneNumber(phoneNumber);
       MyCognito.getInstance(context).userPool.getUser(userPoolPhoneNumber).signOut(); // 부작용은 없는지 체크할 것
       MyCognito.getInstance(context).userPool.getUser(userPoolPhoneNumber).getSessionInBackground(authenticationHandler);
    }

    protected void onResetPasswordClick(String phoneNumber) {
        String userPoolPhoneNumber = CognitoUtils.convertToCompatiblePhoneNumber(phoneNumber);
       MyCognito.getInstance(context).userPool.getUser(userPoolPhoneNumber).forgotPasswordInBackground(forgotPasswordHandler);
    }


    public void onLogin() {
        startActivity(new Intent(context, Welcome.class));
        finish();
    }

    public void onResetPassword() {
        startActivity(new Intent(context, Welcome.class));
        finish();
    }

    public void onClick(View view) {

        if (view.getId() == R.id.userLogin_login) {
            if (Empty.isEmpty(context, etPassword, "비밀번호가 입력되지 않았습니다")) return;
            if (Empty.isEmpty(context, etPhoneNumber, "전화번호가 입력되지 않았습니다")) return;
            onLoginClick(BaseUtils.toSimpleForm(etPhoneNumber.getText().toString()));

        } else if (view.getId() == R.id.userLogin_resetPassword) {
            if (Empty.isEmpty(context, etPhoneNumber, "전화번호가 입력되지 않았습니다")) return;
            new MyAlertDialog(context, "비밀번호 초기화", "문자 인증으로 비밀번호를 초기화 하시겠습니까?\n\n반드시 3분내에 입력하세야 합니다") {
                @Override
                public void yes() {
                    onResetPasswordClick(BaseUtils.toSimpleForm(etPhoneNumber.getText().toString()));
                }
            };
        }
    }

    AuthenticationHandler authenticationHandler = new AuthenticationHandler() {
        private String userPoolPhoneNumber;
        private String passwordInput;
        private Boolean isAuthenticatedNewly = false;

        @Override
        public void onSuccess(CognitoUserSession cognitoUserSession, CognitoDevice device) {
           MyCognito.getInstance(context).setUserSession(context, cognitoUserSession);
            if (isAuthenticatedNewly) {
                myDB.register(new MyRecord(BaseUtils.toSimpleForm(userPoolPhoneNumber), passwordInput, true));
            } else {
                String phoneNumber = BaseUtils.toSimpleForm(etPhoneNumber.getText().toString());
                String password = BaseUtils.encode(etPassword.getText().toString(), phoneNumber);
                myDB.register(new MyRecord(phoneNumber, password, true));
            }
            MyUI.toastSHORT(context, String.format("로그인에 성공했습니다"));
            progressDialog.dismiss();
            onLogin();
        }

        @Override
        public void getAuthenticationDetails(AuthenticationContinuation authenticationContinuation, String username) {
            userPoolPhoneNumber = username;
            passwordInput = BaseUtils.encode(etPassword.getText().toString(), BaseUtils.toSimpleForm(userPoolPhoneNumber));
            getUserAuthentication(authenticationContinuation, userPoolPhoneNumber, passwordInput);
            isAuthenticatedNewly = true;
        }

        @Override
        public void getMFACode(MultiFactorAuthenticationContinuation multiFactorAuthenticationContinuation) {
        }

        @Override
        public void onFailure(Exception exception) {
            String userErrorMessage = ServiceErrorHelper.getCognitoIdErrorMessageForUser(exception, true);
            MyUI.toast(context, userErrorMessage);
            MyUI.toastSHORT(context, String.format("신규 유저이시면 이전 페이지로 가서 '새 고객' 버튼을 눌러주세요"));
            etPassword.setText("");
            progressDialog.dismiss();
        }

        @Override
        public void authenticationChallenge(ChallengeContinuation continuation) {
        }
    };


    private void getUserAuthentication(AuthenticationContinuation continuation, String usernameAlias, String password) {
        AuthenticationDetails authenticationDetails = new AuthenticationDetails(usernameAlias, password, null);
        continuation.setAuthenticationDetails(authenticationDetails);
        continuation.continueTask();
    }

    ForgotPasswordHandler forgotPasswordHandler = new ForgotPasswordHandler() {
        String newPassword = "";
        Boolean isCancelledByUser = false;

        @Override
        public void getResetCode(ForgotPasswordContinuation continuation) {
            Log.i("DEBUG_TAG", "[DEBUG] Password Reset Code: Destination=" + continuation.getParameters().getDestination());
            new PasswordResetAuthDialog(context, continuation).show();
        }

        // PasswordResetAuthDialog가 정상적으로 종료하면 호출됨
        @Override
        public void onSuccess() {
            final String phoneNumber = BaseUtils.toSimpleForm(etPhoneNumber.getText().toString());
            myDB.resetPassword(phoneNumber, newPassword, true);
            new MyDelayTask(context, 100) {
                @Override
                public void onFinish() {
                    onResetPassword();
                    Log.i("DEBUG_TAG", "[DEBUG] 비밀번호가 초기화되었습니다[" + phoneNumber + "]: " + newPassword);
                }
            };
        }

        public void onFailure(Exception exception) {
            String userErrorMessage;
            if (isCancelledByUser) {
                userErrorMessage = "사용자에 의해 취소되었습니다.";
            } else {
                userErrorMessage = ServiceErrorHelper.getCognitoIdErrorMessageForUser(exception, true);
            }
            MyUI.toast(context, userErrorMessage);
            Log.e("DEBUG_JS", String.format("[UserLogIn.onFailure] %s", exception.getLocalizedMessage()));
        }

        class PasswordResetAuthDialog extends BaseDialog {
            private EditText etAuthenCode, etPassword;
            private ForgotPasswordContinuation continuation;
            private LinearLayout passwordContainer;

            public PasswordResetAuthDialog(Context context, ForgotPasswordContinuation continuation) {
                super(context);
                this.continuation = continuation;
                setOnClick(R.id.dialog_input_authenCode_apply);
                setOnClick(R.id.dialog_input_authenCode_cancel);
                setOnClick(R.id.dialog_input_authenCode_clear);
                setOnClick(R.id.dialog_input_authenCode_password_clear);
                passwordContainer = (LinearLayout) findViewById(R.id.dialog_input_authenCode_password_container);

                etAuthenCode = (EditText) findTextViewById(R.id.dialog_input_authenCode_edit, "");
                etPassword = (EditText) findTextViewById(R.id.dialog_input_authenCode_password_edit, "");
            }

            @Override
            public int getThisView() {
                return R.layout.dialog_input_authencode;
            }

            @Override
            public void onClick(View view) {
                if (view.getId() == R.id.dialog_input_authenCode_password_clear) {
                    etPassword.setText("");
                } else if (view.getId() == R.id.dialog_input_authenCode_cancel) {
                    isCancelledByUser = true;
                    continuation.continueTask();
                    MyUI.toastSHORT(context, String.format("비밀번호 초기화를 중지하겠습니다"));
                    dismiss();
                } else if (view.getId() == R.id.dialog_input_authenCode_apply) {
                    if (Empty.isEmpty(context, etAuthenCode, "인증번호가 입력되지 않았습니다")) return;
                    if (Empty.isEmpty(context, etPassword, "비밀번호가 입력되지 않았습니다")) return;
                    // 전화번호 혹은 유저가 입력한 번호를 다시 encode해서 사용한다
                    newPassword = BaseUtils.encode(etPassword.getText().toString().trim(), BaseUtils.toSimpleForm(etPhoneNumber.getText().toString()));
                    continuation.setPassword(newPassword);
                    continuation.setVerificationCode(etAuthenCode.getText().toString());
                    continuation.continueTask();
                    MyUI.toastSHORT(context, String.format("비밀번호를 초기화 했습니다"));
                    dismiss();
                } else if (view.getId() == R.id.dialog_input_authenCode_clear) {
                    etAuthenCode.setText("");
                }
            }
        }
    };

    @Override
    public void onMyMenuCreate() {

    }

    @Override
    protected void onMyMenuSelected(MyMenu myMenu) {

    }

}
