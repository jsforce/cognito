<<<<<<< HEAD
package com.teampls.cognito.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.amazonaws.mobile.client.AWSMobileClient;
import com.amazonaws.mobile.client.Callback;
import com.amazonaws.mobile.client.UserStateDetails;
import com.amazonaws.mobile.config.AWSConfiguration;
import com.amazonaws.mobileconnectors.appsync.AWSAppSyncClient;
import com.amazonaws.mobileconnectors.cognito.CognitoSyncManager;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.regions.Region;
import com.amazonaws.services.s3.AmazonS3Client;
import com.teampls.cognito.R;
import com.teampls.cognito.aws.MyAWSConfigs;
import com.teampls.cognito.aws.MyS3;
import com.teampls.cognito.lib.BaseActivity;
import com.teampls.cognito.lib.MyDevice;
import com.teampls.cognito.lib.MyMenu;
import com.teampls.cognito.lib.MyOnTask;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

public class MainView extends BaseActivity {

    public static void startActivity(Context context) {
        context.startActivity(new Intent(context, MainView.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setOnClick(R.id.main_put);
        Log.i("DEBUG_JS", String.format("[MyS3.upload] %s", AWSMobileClient.getInstance().getConfiguration().toString()));
    }


    @Override
    protected void onMyMenuSelected(MyMenu myMenu) {

    }

    @Override
    public int getThisView() {
        return R.layout.activity_mainview;
    }

    @Override
    public void onMyMenuCreate() {

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.main_put:
                onPutClick();
                break;
        }
    }

    private void onPutClick() {
        Log.i("DEBUG_JS", String.format("[MainView.onPutClick] %s", MyDevice.teamplDir+"/download.txt"));
        MyS3.getInstance(context).download("public/sample.txt", MyDevice.teamplDir+"/download.txt", new MyOnTask() {
            @Override
            public void onTaskDone(Object result) {

            }
        });
    }
}
=======
package com.teampls.cognito.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.amazonaws.mobile.client.AWSMobileClient;
import com.amazonaws.mobile.client.Callback;
import com.amazonaws.mobile.client.UserStateDetails;
import com.teampls.cognito.R;
import com.teampls.cognito.lib.BaseActivity;
import com.teampls.cognito.lib.MyMenu;

public class MainView extends BaseActivity {

    public static void startActivity(Context context) {
        context.startActivity(new Intent(context, MainView.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    protected void onMyMenuSelected(MyMenu myMenu) {

    }

    @Override
    public int getThisView() {
        return R.layout.activity_mainview;
    }

    @Override
    public void onMyMenuCreate() {

    }

    @Override
    public void onClick(View v) {

    }
}
>>>>>>> 3e624cd229b12a411aea4173409a410e8cde6b49
