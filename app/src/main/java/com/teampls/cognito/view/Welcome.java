package com.teampls.cognito.view;

import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;

import com.amazonaws.mobile.client.AWSMobileClient;
import com.amazonaws.mobile.client.Callback;
import com.amazonaws.mobile.client.UserStateDetails;
import com.amazonaws.mobile.client.results.SignInResult;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferService;
import com.teampls.cognito.R;
import com.teampls.cognito.aws.MyAWSConfigs;
import com.teampls.cognito.aws.MyCognito;
import com.teampls.cognito.lib.BaseActivity;
import com.teampls.cognito.lib.BaseAppWatcher;
import com.teampls.cognito.lib.MyDB;
import com.teampls.cognito.lib.MyDevice;
import com.teampls.cognito.lib.MyMenu;
import com.teampls.cognito.lib.MyUI;
import com.teampls.cognito.view_login.UserConfirm;
import com.teampls.cognito.view_login.UserIsRegistered;
import com.teampls.cognito.view_login.UserReqPermissions;

public class Welcome extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        MyAWSConfigs.isProduction();
        myDB = MyDB.getInstance(context);
        myDB.open();
        init();
    }

    protected void init() {
        // 권한이 필요한 기능이 있으면 안된다.
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        boolean isProduction = MyAWSConfigs.isProduction();
        BaseAppWatcher.init(context, isProduction, "cognito");
        getApplicationContext().startService(new Intent(getApplicationContext(), TransferService.class));

        AWSMobileClient.getInstance().initialize(getApplicationContext(), new Callback<UserStateDetails>() {
            @Override
            public void onResult(UserStateDetails result) {
                selectNextActivity();
            }

            @Override
            public void onError(Exception e) {
                MyUI.toastSHORT(context, String.format("오류 발생 : %s", e.getLocalizedMessage()));
            }
        });
    }

    protected void selectNextActivity() {
        if (myDB.confirmed()) {
            //AWSMobileClient.getInstance().signOut();
            AWSMobileClient.getInstance().signIn(myDB.getUserPoolNumber(), myDB.getPassword(), null, new Callback<SignInResult>() {
                @Override
                public void onResult(SignInResult result) {
                    switch (result.getSignInState()) {
                        case DONE:
                            Log.i("DEBUG_JS", String.format("[Welcome.onResult] sign in? %s", AWSMobileClient.getInstance().isSignedIn()));
                            MainView.startActivity(context); // 사용자
                            break;
                        default:
                            MyUI.toastSHORT(context, String.format("로그인 실패"));
                            break;
                    }
                }

                @Override
                public void onError(Exception e) {
                    if (e.getLocalizedMessage().contains("User does not exist.")) {
                        MyUI.toastSHORT(context, String.format("유저가 존재하지 않습니다"));
                    } else {
                        Log.e("DEBUG_JS", String.format("[Welcome.onError] %s", e.getLocalizedMessage()));
                        MyUI.toastSHORT(context, String.format("로그인 실패\n%s", e.getLocalizedMessage()));
                    }
                }
            });


        } else if (myDB.registered()) {
            UserConfirm.startActivity(context);
            finish();
        } else {
            if (MyDevice.hasAllPermissions(context)) {
                UserIsRegistered.startActivity(context); // 가입고객? 새고객?
                finish();
            } else {
                UserReqPermissions.startActivity(context); // 권한추가부터
                finish();
            }
        }
    }

    @Override
    protected void onMyMenuSelected(MyMenu myMenu) {

    }

    @Override
    public int getThisView() {
        return R.layout.activity_welcome;
    }

    @Override
    public void onMyMenuCreate() {

    }

    @Override
    public void onClick(View v) {

    }
}
