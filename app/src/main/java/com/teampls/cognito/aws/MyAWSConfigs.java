package com.teampls.cognito.aws;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.AnonymousAWSCredentials;
import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.auth.CognitoCredentialsProvider;
import com.amazonaws.mobile.config.AWSConfiguration;
import com.amazonaws.mobileconnectors.appsync.AWSAppSyncClient;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUserPool;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUserSession;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtilityOptions;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.cognitoidentityprovider.AmazonCognitoIdentityProvider;
import com.amazonaws.services.cognitoidentityprovider.AmazonCognitoIdentityProviderClient;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.apollographql.apollo.GraphQLCall;


import java.util.HashMap;
import java.util.Map;

import javax.annotation.Nonnull;

public class MyAWSConfigs {

    /// SERVER SWITCH //////////////////////////
    public static Boolean isProduction = true;
    public static Boolean isMaster = true;
    ////////////////////////////////////////////

    ////////////  COMMON /////////////////////////////////////////////////////////
    public static final Regions USER_POOL_REGION = Regions.AP_NORTHEAST_2;
    public static final Regions IDENTITY_POOL_REGION = Regions.AP_NORTHEAST_2;
    private static final String PUBLIC_API_ROOT_URL = "https://72miou2vd7.execute-api.ap-northeast-2.amazonaws.com/prod/sps";
    private static final String PUBLIC_API_KEY = "mT0nB6yCXf5rzFGYXN32n6go4MQW7KTJ9mucJ2OZ";
    public static final Regions S3_BUCKET_REGIONS = Regions.AP_NORTHEAST_2;
////////////////////////////////////////////////////////////////////////

    ////////////  PROD /////////////////////////////////////////////////////////
    public static String prodUserpoolId = "ap-northeast-2_DwCZdBvLG";
    public static String prodUserpoolAppId = "5b4hau5qi5h53j71bbom26f4n2";
    public static String prodUserpoolAppSecret = "d9sd2dbleo98kp2lbepeo7tndb22dv5stgmt259kp90vlm4lf8h";
    public static String prodIdentitypoolId = "ap-northeast-2:c36c6e3f-72a4-4b72-b07e-232ba9720c82";
    ////////////////////////////////////////////////////////////////////////

    ////////////  DEV /////////////////////////////////////////////////////////
    public static String devUserpoolId = "";
    public static String devUserpoolAppId = "";
    public static String devUserpoolAppSecret = "";
    public static String devIdentitypoolId = "";
    ////////////////////////////////////////////////////////////////////////

    public static String s3Name = "jsforcebucket01", s3SlipsBucketName = "", getS3ItemsBucketName = "", s3ReceiptBucketName = "";
    public static String apiGatewayURL = "";

    public static Boolean isProduction() {
        return isProduction;
    }

    public static final int REMOTE_ITEM_PRICE_UNIT = 1000;     // 원격에서는 1천원을 1 단위로 저장/전송한다.

    public Regions getUserPoolRegion() {
        return MyAWSConfigs.USER_POOL_REGION;
    }

    public Regions getIdentityPoolRegion() {
        return MyAWSConfigs.IDENTITY_POOL_REGION;
    }

    public String getApigatewayApiKey() {
        return null;
    }

    public String getPublicApiRootUrl() {
        return MyAWSConfigs.PUBLIC_API_ROOT_URL;
    }

    public String getPublicApiKey() {
        return MyAWSConfigs.PUBLIC_API_KEY;
    }

    public Regions getS3BucketRegion() {
        return MyAWSConfigs.S3_BUCKET_REGIONS;
    }

    public static String getResourcePath(String resourceRelativePath) {
        return apiGatewayURL + resourceRelativePath;
    }

    public static String getResourcePath(String... resourceRelativePath) {
        String result = apiGatewayURL;
        for (String path : resourceRelativePath) {
            result += ("/" + path);
        }
        return result;
    }

    public static class MyCognito {

    }

//    class Temp {
//        private AWSAppSyncClient mAWSAppSyncClient;
//        protected void onCreate(Bundle savedInstanceState) {
//            mAWSAppSyncClient = AWSAppSyncClient.builder()
//                    .context(applicationContext)
//                    .awsConfiguration(new AWSConfiguration(applicationContext))
//                    .build();
//        }
//
//
//    }
}
