package com.teampls.cognito.aws;

import android.content.Context;
import android.util.Log;

import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.mobileconnectors.cognito.CognitoSyncManager;
import com.amazonaws.mobileconnectors.cognito.Dataset;

/**
 * 공용 사용자 데이터 관리자
 *
 * @author lucidite
 */

public class CognitoUserDataManager {
    private static CognitoUserDataManager instance = new CognitoUserDataManager();

    public static CognitoUserDataManager getInstance() {
        return instance;
    }

    private Dataset dataset;
    private CognitoSyncManager client;

    public void sync(Context context, Dataset.SyncCallback syncCallback) {
        this.client = this.getSyncManager(context);
        if (client == null) {
            return;
        }
        this.dataset = client.openOrCreateDataset("user_data");
        this.dataset.synchronize(syncCallback); // allows your application to store data in the cloud
    }

    public String getTransactionBaseData(CognitoTransactionFields field) {
        String value = this.dataset.get(field.fieldStr);
        return (value == null) ? "" : value;
    }

    public void setTransactionBaseData(CognitoTransactionFields field, String newValue, Dataset.SyncCallback syncCallback) {
        if (newValue == null || newValue.isEmpty()) {
            this.dataset.remove(field.fieldStr);
        } else {
            this.dataset.put(field.fieldStr, newValue);
        }
        this.dataset.synchronize(syncCallback);
    }

    private CognitoSyncManager getSyncManager(Context context) {
        CognitoCachingCredentialsProvider provider = (CognitoCachingCredentialsProvider)MyCognito.getInstance(context).getCredentialProvider();
        if (provider == null) {
            Log.e("DEBUG_TAG", "[ERROR/USERDATA] Credential Provider = null");
            return null;
        }
        return new CognitoSyncManager(context, MyAWSConfigs.USER_POOL_REGION, provider);
    }
}
