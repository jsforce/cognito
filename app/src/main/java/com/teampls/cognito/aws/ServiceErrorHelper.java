package com.teampls.cognito.aws;

/**
 * 서비스 관련 에러 처리를 위한 유틸리티 클래스
 *
 * @author lucidite
 */

import android.util.Log;

import com.amazonaws.AmazonServiceException;

public class ServiceErrorHelper {
    private static void repeatInLogCat(Exception e) {
        Log.i("DEBUG_TAG", "[DEBUG] " + e.getClass().toString());
        Log.i("DEBUG_TAG", "[DEBUG] " + e.getLocalizedMessage());
    }

    /**
     * AWS Cognito 관련 예외에 대한 사용자에게 표시하기 위한 에러 메시지를 얻는다.
     *
     * @param e 발생한 예외 인스턴스
     * @param repeatInLogCat    true일 경우 LogCat에 에러 관련 내용을 표시한다(tag=DEBUG_TAG, level=I)
     * @return  사용자에게 표시할 에러 정보 메시지
     */
    public static String getCognitoIdErrorMessageForUser(Exception e, Boolean repeatInLogCat) {
        if (repeatInLogCat) {
            repeatInLogCat(e);
        }
        if (AmazonServiceException.class.isInstance(e)) {
            AmazonServiceException awsException = (AmazonServiceException) e;
            switch (awsException.getErrorCode()) {
                // 일반적으로 발생할 수 있는 것으로 기대되는 예외 목록
                case "AliasExistsException": return "이미 등록된 전화번호입니다.";
                case "UserNotFoundException": return "등록된 사용자를 찾을 수 없습니다.";
                case "CodeMismatchException": return "인증 코드가 일치하지 않습니다.";
                case "ExpiredCodeException": return "만료된 인증 코드입니다.";
                case "InvalidParameterException": return "입력값에 오류가 있습니다.";
                case "InvalidPasswordException": return "비밀번호가 올바르지 않습니다.";
                case "LimitExceededException": return "제한시간 내 시도 횟수를 초과하였습니다.\n나중에 다시 시도해 주십시오.";
                case "TooManyFailedAttemptsException": return "여러 번 실패하였습니다.\n초기화가 필요합니다.";
                // 발생할 수 있을 것 같으나 일반적으로는 발생이 기대되지 않는 예외 목록
                case "CodeDeliveryFailureException": return "인증 코드를 전송할 수 없습니다.";
                case "TooManyRequestsException": return "너무 많은 처리를 요청하셨습니다.";
                // 사용하지 않는 기능 관련 예외 목록. 차후 기능 보강 시에 발생 가능
                case "UserNotConfirmedException": return "인증되지 않은 사용자입니다.";
                case "PasswordResetRequiredException": return "비밀번호를 초기화해야 합니다.";
                case "MFAMethodNotFoundException": return "2단계 인증 수단을 찾지 못하였습니다.";
                case "UserImportInProgressException": return "서버에서 사용자 관리 작업을 진행하고 있습니다.";
                case "NotAuthorizedException": return "사용자 권한을 확인할 수 없습니다.\n다시 로그인해 주십시오.";
                // 사용자 Lambda 관련 예외 목록
                case "UserLambdaValidationException": return "서버에서 사용자 유효성 확인에 실패했습니다.";
                case "InvalidLambdaResponseException": return "서버 에러가 발생하였습니다.";
                // 서버 에러 및 서버 설정 관련 문제에 기인하는 예외 목록
                case "InternalErrorException": return "서버 에러 또는 앱내 충돌이 발생하였습니다.";
                case "InvalidEmailRoleAccessPolicyException": return "서버 에러로 이메일을 전송할 수 없습니다.";
                case "InvalidSmsRoleAccessPolicyException": return "서버 에러로 인증 코드를 전송할 수 없습니다.";
                case "InvalidSmsRoleTrustRelationshipException": return "서버 에러로 인증 코드를 전송할 수 없습니다.";
                case "InvalidUserPoolConfigurationException": return "사용자 관련 서버 에러가 발생하였습니다.";
                case "PreconditionNotMetException": return "요청을 처리하기 위한 사전 조건을 충족하지 않습니다.";
                case "ResourceNotFoundException": return "서버 에러로 요청을 처리할 수 없습니다.";
                case "UnexpectedLambdaException": return "서버에서 요청을 처리하던 중 에러가 발생하였습니다.";
                default: return "요청을 처리하던 중에 예외 상황이 발생하였습니다.";
            }
        } else {    // Not an AmazonServiceException
            return "요청을 처리하던 중에 에러가 발생하였습니다.";
        }
    }

    /**
     * 서비스 관련 예외에 대한 사용자에게 표시하기 위한 에러 메시지를 얻는다.
     *
     * @param e 발생한 예외 인스턴스
     * @param repeatInLogCat    true일 경우 LogCat에 에러 관련 내용을 표시한다(tag=DEBUG_TAG, level=I)
     * @return  사용자에게 표시할 에러 정보 메시지
     */
    public static String getMyServiceFailureMessageForUser(Exception e, Boolean repeatInLogCat) {
        if (repeatInLogCat) {
            repeatInLogCat(e);
        }
        if (MyServiceFailureException.class.isInstance(e)) {
            MyServiceFailureException serviceFailureException = (MyServiceFailureException) e;
            switch (serviceFailureException.getErrorCode()) {
                // TODO @SunstriderAmazonServiceException 관련 에러 메시지 상세화
                case "AmazonServiceException":  return "서버 서비스 에러가 발생했습니다.";
                case "JSONException": return "데이터 형식 관련 오류가 있습니다.";
                case "IOException": return "입출력 관련 오류가 발생하였습니다.";
                case "HttpError[400]": return "올바르지 않은 서비스 요청입니다.";
                case "HttpError[404]": return "요청하신 데이터를 찾을 수 없습니다.";
                default:
                    if (serviceFailureException.getLocalizedMessage().contains("[403]ConditionalCheckFailedException"))
                        return "입력값이 올바르지 않습니다";
                    else
                        return "서비스 요청을 처리하는 중에 에러가 발생하였습니다.";
            }
        } else {    // Not a MyServiceFailureException
            return "서비스 요청을 처리하는 중에 알 수 없는 에러가 발생하였습니다.";
        }
    }
}
