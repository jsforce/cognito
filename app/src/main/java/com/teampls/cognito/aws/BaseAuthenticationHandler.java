package com.teampls.cognito.aws;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.mobileconnectors.cognito.Dataset;
import com.amazonaws.mobileconnectors.cognito.Record;
import com.amazonaws.mobileconnectors.cognito.SyncConflict;
import com.amazonaws.mobileconnectors.cognito.exceptions.DataStorageException;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoDevice;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUserSession;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.continuations.AuthenticationContinuation;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.continuations.AuthenticationDetails;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.continuations.ChallengeContinuation;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.continuations.MultiFactorAuthenticationContinuation;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.handlers.AuthenticationHandler;
import com.teampls.cognito.lib.BaseAppWatcher;
import com.teampls.cognito.lib.MyDB;
import com.teampls.cognito.lib.MyOnTask;
import com.teampls.cognito.lib.MyUI;

import java.util.List;

/**
 * Created by Medivh on 2017-01-27.
 */

abstract public class BaseAuthenticationHandler implements AuthenticationHandler, MyOnTask {
    protected Context context;
    private String userPoolPhoneNumber;
    private String passwordInput;
    private Boolean isAuthenticatedNewly = false;

    public BaseAuthenticationHandler(Context context) {
        this.context = context;
    }

    @Override
    public void onSuccess(CognitoUserSession cognitoUserSession, CognitoDevice device) {
        Log.i("DEBUG_JS", "인증성공");
       MyCognito.getInstance(context).setUserSession(context, cognitoUserSession);
        if (userPoolPhoneNumber == null)
            userPoolPhoneNumber = MyDB.getInstance(context).getUserPoolNumber();
        BaseAppWatcher.setLoginState(context, LoginState.onSuccess);
        onTaskDone(null);
        Log.i("DEBUG_JS", "[DEBUG/USER] Token(After)=" +MyCognito.getInstance(context).getIdentityToken());

        // 사용자 인증이 완료된 후 데이터 관리자를 동기화해야 한다.
        CognitoUserDataManager.getInstance().sync(this.context, new Dataset.SyncCallback() {
            @Override
            public void onSuccess(Dataset dataset, List<Record> updatedRecords) {

            }
            @Override
            public boolean onConflict(Dataset dataset, List<SyncConflict> conflicts) { return false; }
            @Override
            public boolean onDatasetDeleted(Dataset dataset, String datasetName) { return false; }
            @Override
            public boolean onDatasetsMerged(Dataset dataset, List<String> datasetNames) { return false; }
            @Override
            public void onFailure(DataStorageException dse) {
                Log.e("DEBUG_JS", "[ERROR/USERDATA] 동기화 실패: " + dse.getMessage());
            }
        });
    }

    @Override
    public void getAuthenticationDetails(AuthenticationContinuation authenticationContinuation, String username) {
        Log.i("DEBUG_JS", "[DEBUG/USER] getAuthenticationDetails에 진입했습니다: " + username);
        userPoolPhoneNumber = username;
        passwordInput =  MyDB.getInstance(context).getPassword();
        getUserAuthentication(authenticationContinuation, userPoolPhoneNumber, passwordInput);
        isAuthenticatedNewly = true;
    }

    private void getUserAuthentication(AuthenticationContinuation continuation, String usernameAlias, String password) {
        AuthenticationDetails authenticationDetails = new AuthenticationDetails(usernameAlias, password, null);
        continuation.setAuthenticationDetails(authenticationDetails);
        continuation.continueTask();
    }

    @Override
    public void getMFACode(MultiFactorAuthenticationContinuation continuation) {

    }

    @Override
    public void authenticationChallenge(ChallengeContinuation continuation) {

    }

   public void onNotAuthorized(){};

    @Override
    public void onFailure(Exception exception) {
        final Activity activity = (Activity) context;
        if (exception instanceof AmazonServiceException) {
            switch (((AmazonServiceException) exception).getErrorCode()) {
                default:
                    String userErrorMessage = ServiceErrorHelper.getCognitoIdErrorMessageForUser(exception, true);
                    MyUI.toast( activity, userErrorMessage);
                    BaseAppWatcher.setLoginState(context, LoginState.onAwsError);
                    break;
                case "NotAuthorizedException":
                    BaseAppWatcher.setLoginState(context, LoginState.notAuthorized);
                    onNotAuthorized();
                    break;
                case "UserNotFoundException":
                    Log.e("DEBUG_JS", String.format("[BaseAuthenticationHandler.onFailure] user %s not found in AWS", MyDB.getInstance(context).getPrivateName()));
                    BaseAppWatcher.setLoginState(context, LoginState.userNotFound);
                    break;
            }
        } else {
            BaseAppWatcher.setLoginState(context, LoginState.onError);
            Log.e("DEBUG_JS", String.format("[Welcome.onFailure] %s", exception.getLocalizedMessage()));
            //   String message = MyDevice.isOnline() ? "통신은 정상적이나 다른 문제가 발생했습니다" : "통신에서 문제가 발견되었습니다\n와이파이 상태 등을 점검해주세요";
        }
    }

}
