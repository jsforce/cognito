package com.teampls.cognito.aws;

import android.content.Context;
import android.util.Log;

import com.amazonaws.mobile.client.AWSMobileClient;
import com.amazonaws.mobile.config.AWSConfiguration;
import com.amazonaws.mobileconnectors.appsync.AWSAppSyncClient;
import com.amazonaws.mobileconnectors.appsync.S3ObjectManagerImplementation;
import com.amazonaws.mobileconnectors.appsync.fetcher.AppSyncResponseFetchers;
import com.amazonaws.regions.Region;
import com.amazonaws.services.s3.AmazonS3Client;
import com.apollographql.apollo.GraphQLCall;
import com.apollographql.apollo.exception.ApolloException;

import javax.annotation.Nonnull;

public class MyAwsDB {
    private static MyAwsDB instance;
    private Context context;
    private AWSAppSyncClient appSyncClient;

    public static MyAwsDB getInstance(Context context) {
        if (instance == null)
            instance = new MyAwsDB(context);
        return instance;
    }

    private MyAwsDB(Context context) {
        this.context = context;
        appSyncClient = AWSAppSyncClient.builder()
                .context(context)
                .awsConfiguration(new AWSConfiguration(context)) // AWSConfiguration() reads configuration information in the awsconfiguration.json file
                .s3ObjectManager(new S3ObjectManagerImplementation(new AmazonS3Client(AWSMobileClient.getInstance(),  Region.getRegion(MyAWSConfigs.S3_BUCKET_REGIONS))))
                .build();
    }

    public void query(){
        appSyncClient.query(ListTodosQuery.builder().build())
                .responseFetcher(AppSyncResponseFetchers.CACHE_AND_NETWORK)
                .enqueue(todosCallback);
    }

    //GraphQLCall.Callback<{NAME}Query.Data>
    // where {NAME} comes from the GraphQL statements that amplify codegen created after you ran a Gradle build.

    private GraphQLCall.Callback<ListTodosQuery.Data> todosCallback = new GraphQLCall.Callback<ListTodosQuery.Data>() {
        @Override
        public void onResponse(@Nonnull Response<ListTodosQuery.Data> response) {
            Log.i("DEBUG_JS", response.data().listTodos().items().toString());
        }

        @Override
        public void onFailure(@Nonnull ApolloException e) {
            Log.e("DEBUG_JS", e.toString());
        }
    };
}
