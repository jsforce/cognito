package com.teampls.cognito.aws;

import android.content.Context;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.AnonymousAWSCredentials;
import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.auth.CognitoCredentialsProvider;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUserPool;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUserSession;
import com.amazonaws.regions.Region;
import com.amazonaws.services.cognitoidentityprovider.AmazonCognitoIdentityProvider;
import com.amazonaws.services.cognitoidentityprovider.AmazonCognitoIdentityProviderClient;

import java.util.HashMap;
import java.util.Map;

public class MyCognito {
    private static MyCognito instance;
    private Context context;
    public CognitoCredentialsProvider credentialsProvider;
    public CognitoUserPool userPool;
    private static CognitoUserSession userSession;
    public String userpoolId = "", userpoolAppId = "", userpoolAppSecret = "", identitypoolId = ""; // cognito config

    public static MyCognito getInstance(Context applicationContext) {
        if (instance == null)
            instance = new MyCognito(applicationContext);
        return instance;
    }

    public MyCognito(Context applicationContext) {
        this.context = applicationContext;
        if (MyAWSConfigs.isProduction) {
            userpoolId = MyAWSConfigs.prodUserpoolId;
            userpoolAppId = MyAWSConfigs.prodUserpoolAppId;
            userpoolAppSecret = MyAWSConfigs.prodUserpoolAppSecret;
            identitypoolId = MyAWSConfigs.prodIdentitypoolId;
        } else {
            userpoolId = MyAWSConfigs.devUserpoolId;
            userpoolAppId = MyAWSConfigs.devUserpoolAppId;
            userpoolAppSecret = MyAWSConfigs.devUserpoolAppSecret;
            identitypoolId = MyAWSConfigs.devIdentitypoolId;
        }

    }

    public CognitoCachingCredentialsProvider getCredentialProvider() {
        return (CognitoCachingCredentialsProvider) credentialsProvider;
    }

    private void updateCredentialProvider() {
        credentialsProvider = new CognitoCachingCredentialsProvider(context, identitypoolId, MyAWSConfigs.IDENTITY_POOL_REGION);
        Map<String, String> logInMap = new HashMap<>();
        String loginKey = "cognito-idp." + MyAWSConfigs.USER_POOL_REGION.getName() + ".amazonaws.com/" + userpoolId;
        logInMap.put(loginKey, userSession.getIdToken().getJWTToken());
        credentialsProvider.setLogins(logInMap);
    }

    private void updateUserPool() {
        AmazonCognitoIdentityProvider cipClient = new AmazonCognitoIdentityProviderClient(
                credentialsProvider.getCredentials(), new ClientConfiguration());
        cipClient.setRegion(Region.getRegion(MyAWSConfigs.USER_POOL_REGION));
        userPool = new CognitoUserPool(context, userpoolId, userpoolAppId, userpoolAppSecret, cipClient);
    }

    public void init() {
        if (userPool == null) {
            AmazonCognitoIdentityProvider cipClient = new AmazonCognitoIdentityProviderClient(new AnonymousAWSCredentials(), new ClientConfiguration());
            cipClient.setRegion(Region.getRegion(MyAWSConfigs.USER_POOL_REGION));
            userPool = new CognitoUserPool(context, userpoolId, userpoolAppId, userpoolAppSecret, cipClient);
//            Log.i("DEBUG_JS", String.format("[MyCognitoUser.init] user pool : userPoolId %s, clientId %s, secret %s, region %s",
//                    MyAWSConfigs.getCurrentConfig().getUserPoolId(), MyAWSConfigs.getCurrentConfig().getUserPoolClientId(),
//                    MyAWSConfigs.getCurrentConfig().getUserPoolClientSecret(), Region.getRegion(MyAWSConfigs.getCurrentConfig().getUserPoolRegion()
//                    )));
        }
    }

    public void setUserSession(Context context, CognitoUserSession cognitoUserSession) {
        this.context = context;
        userSession = cognitoUserSession;
        updateCredentialProvider();
        updateUserPool();
    }

    public static String getIdentityToken() {
        //Log.i("DEBUG_TAG", "TOKEN====> " + getCurrSession().getIdToken().getJWTToken());
        return userSession.getIdToken().getJWTToken();
    }

}
