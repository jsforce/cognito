package com.teampls.cognito.aws;

import android.content.Context;
import android.util.Log;

import com.amazonaws.mobile.client.AWSMobileClient;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtilityOptions;
import com.amazonaws.regions.Region;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.teampls.cognito.lib.MyOnTask;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

public class MyS3 {
    private static MyS3 instance;
    private TransferUtility transferUtility;
    private Context context;

    public static MyS3 getInstance(Context applicationContext) {
        if (instance == null)
            instance = new MyS3(applicationContext);
        return instance;
    }

    private MyS3(Context context) {
        this.context = context;
        TransferUtilityOptions options = new TransferUtilityOptions();
        options.setTransferThreadPoolSize(10); // 10 thread
        transferUtility = TransferUtility.builder()
                .context(context)
                .awsConfiguration(AWSMobileClient.getInstance().getConfiguration())
                .s3Client(new AmazonS3Client(AWSMobileClient.getInstance(), Region.getRegion(MyAWSConfigs.S3_BUCKET_REGIONS)))
                .transferUtilityOptions(options)
                .build();
    }

    private void createSampleFile() {
        File file = new File(context.getFilesDir(), "sample.txt");
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(file));
            writer.append("Howdy World!");
            writer.close();
        }
        catch(Exception e) {
            Log.e("DEBUG_JS", String.format("[MainView.onPutClick] %s", e.getLocalizedMessage()));
        }
    }

    public void upload(String s3Path, String localPath) {



        TransferObserver observer = transferUtility.upload(s3Path, new File(localPath));
        observer.setTransferListener(new TransferListener() {
            @Override
            public void onStateChanged(int id, TransferState state) {
                if (TransferState.COMPLETED == state) {
                    Log.i("DEBUG_JS", String.format("[MainView.onPutClick] transferred %d", observer.getBytesTransferred()));
                }
            }

            @Override
            public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                int percent = (int) (100 * (float) bytesCurrent / (float) bytesTotal);
                Log.i("DEBUG_JS", String.format("[MyS3.onProgressChanged] %d %%", percent));
            }

            @Override
            public void onError(int id, Exception ex) {

            }
        });
    }


    public void download(String s3Path, String localPath, MyOnTask onTask) {
        TransferObserver observer = transferUtility.download(s3Path, new File(localPath));
        observer.setTransferListener(new TransferListener() {
            @Override
            public void onStateChanged(int id, TransferState state) {
                if (TransferState.COMPLETED == state) {
                    Log.i("DEBUG_JS", String.format("[MainView.onPutClick] transferred %d", observer.getBytesTransferred()));
                    if (onTask != null)
                        onTask.onTaskDone(true);
                }
            }

            @Override
            public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                int percent = (int) (100 * (float) bytesCurrent / (float) bytesTotal);
                Log.i("DEBUG_JS", String.format("[MyS3.onProgressChanged] %d %%", percent));
            }

            @Override
            public void onError(int id, Exception ex) {

            }
        });
    }

    public void pause(int id) {
        transferUtility.pause(id);
        //전송 중에 앱이 다운되거나 종료된 후 다시 실행됐을 때 일시 중지된 전송이 있는지 판단하여 처리할 수 있도록
        // transfer ID로 getTransfersWithType(transferType)와getTransfersWithTypeAndState(transferType, transferState)을 활용할 수도 있습니다.
    }

    public void resume(int id) {
        transferUtility.resume(id);
    }
}
