package com.teampls.cognito.aws;

/**
 * Created by Medivh on 2018-01-21.
 */

public enum LoginState {
    Default, onTrying, onSuccess, onAwsError, notAuthorized, userNotFound, onError, Disconnected;
}
