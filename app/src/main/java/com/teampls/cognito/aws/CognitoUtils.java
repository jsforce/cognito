package com.teampls.cognito.aws;

import java.util.Calendar;

/**
 * AWS Cognito 관련 유틸리티 함수 클래스
 *
 * @author lucidite
 */
public class CognitoUtils {
    public static final int THE_FIRST_YEAR_OF_SERVICE = 2020;
    public static final String COUNTRY_CODE_KR = "+82";

    /**
     * 사용자 전화번호 입력을 대한민국 국가코드(+82)로 시작하는 전화번호로 변경한다.
     * @param phoneNumberByUser 01로 시작하는 13자리 숫자로 된 전화번호(e.g. 01090125678). 함수 내에서 유효성 체크를 하지 않음에 유의한다.
     * @return
     */
    public static String convertToCompatiblePhoneNumber(String phoneNumberByUser) {
        return COUNTRY_CODE_KR + phoneNumberByUser.substring(1);
    }

    /**
     * 프로젝트 코드네임, 날짜, 시간, 전화번호를 인코딩한 username을 생성한다.
     * @param codename
     * @param today
     * @param phoneNumber 01로 시작하는 13자리 숫자로 된 전화번호(e.g. 01090125678). 함수 내에서 유효성 체크를 하지 않음에 유의한다.
     * @return
     */
    public static String createEncodedUsername(ProjectCodeName codename, Calendar today, String phoneNumber) {
        int encodedTimeKey = ((today.get(Calendar.HOUR_OF_DAY)*60 + today.get(Calendar.MINUTE))*60
                + today.get(Calendar.SECOND)) / 30;
        return String.format("%s%01x%01x%02x%03x%08x",
                codename.toUniliteralCode(),
                today.get(Calendar.YEAR) - THE_FIRST_YEAR_OF_SERVICE,
                today.get(Calendar.MONTH) + 1,      // Calendar에서는 1월이 0으로 시작한다.
                today.get(Calendar.DAY_OF_MONTH),
                encodedTimeKey,
                Integer.valueOf(phoneNumber.substring(2)));
    }

    public enum ProjectCodeName {
        MAYQUEEN("m");

        private String uniliteralCode;

        ProjectCodeName(String uniliteralCode) {
            this.uniliteralCode = uniliteralCode;
        }

        public String toUniliteralCode() {
            return this.uniliteralCode;
        }
    }
}
