package com.teampls.cognito.aws;

/**
 * @author lucidite
 */

public enum CognitoTransactionFields {
    SHOPNAME("tr_shopname"),
    PHONE_1("tr_phone"),
    PHONE_2("tr_phone2"),
    BUILDING_ADDR("tr_building_addr"),
    SHOP_ADDR("tr_shop_addr"),
    BANK_ACCOUNT_1("tr_bankaccount"),
    BANK_ACCOUNT_2("tr_bankaccount2"),
    BANK_ACCOUNT_3("tr_bankaccount3"),
    COMMENTS("tr_comments");

    public String fieldStr;

    CognitoTransactionFields(String fieldStr) {
        this.fieldStr = fieldStr;
    }
}
