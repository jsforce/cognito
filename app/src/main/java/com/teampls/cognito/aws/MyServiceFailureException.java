package com.teampls.cognito.aws;

/**
 * 서비스 실패 예외. 주로 서버에서 클라이언트로 전달하는 예외를 처리하기 위해 사용한다.
 *
 * @author lucidite
 */

public class MyServiceFailureException extends Exception {
    private String errorCode = "";

    public MyServiceFailureException(String errorCode, String errorMessage) {
        super(errorMessage);
        this.errorCode = errorCode;
    }

    public MyServiceFailureException(Exception e) {
        super(e.getMessage());
        e.printStackTrace();
        this.errorCode = e.getClass().getSimpleName();
    }

    public String getErrorCode() {
        return errorCode;
    }

 }

// 에러처리 코드 : ServiceErrorHelper

/* error Code
<로그인>
"NotAuthorizedException" : 로그인시 미권한유저
"UserNotFoundException": 로그인시 미가입유저




 */
